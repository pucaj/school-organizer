﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SzkolaXXII.Common.Const
{
    public static class TimetableConst
    {
        public static List<DateTime> StartLesson()
        {
            return  new List<DateTime>
            {
               DateTime.Parse("8:15"),
               DateTime.Parse("9:15"),
               DateTime.Parse("10:15"),
               DateTime.Parse("11:15"),
               DateTime.Parse("12:15"),
               DateTime.Parse("13:15"),
               DateTime.Parse("14:15"),
               DateTime.Parse("15:05"),
               DateTime.Parse("16:00"),
               DateTime.Parse("16:50"),
               DateTime.Parse("17:40"),
               DateTime.Parse("18:30"),
               DateTime.Parse("19:20"),
               DateTime.Parse("20:10")
            };
        }

        public static List<DateTime> EndLesson()
        {
            return new List<DateTime>
            {
                DateTime.Parse("9:00"),
                DateTime.Parse("10:00"),
                DateTime.Parse("11:00"),
                DateTime.Parse("12:00"),
                DateTime.Parse("13:00"),
                DateTime.Parse("14:00"),
                DateTime.Parse("15:00"),
                DateTime.Parse("15:50"),
                DateTime.Parse("16:45"),
                DateTime.Parse("17:35"),
                DateTime.Parse("18:25"),
                DateTime.Parse("19:15"),
                DateTime.Parse("20:05"),
                DateTime.Parse("20:55")
            };
        }

        public static List<string> Days()
        {
            return new List<string>
            {
                "Poniedziałek",
                "Wtorek",
                "Środa",
                "Czwartek",
                "Piątek",
                "Sobota",
                "Niedziela"
            };
        }

    }
}
