﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SzkolaXXII.Common.Security.Roles
{
    public static class SystemDefaultRoles
    {
        public const string Administrator = "Administrator";
        public const string Moderator = "Moderator";
        public const string Teacher = "Nauczyciel";
        public const string Student = "Student";
    }
}
