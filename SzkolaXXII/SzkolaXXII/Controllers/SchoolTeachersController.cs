﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Common.Security.Roles;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.InputModel;
using SzkolaXXII.Web.Models.School;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.Models
{
    public class SchoolTeachersController : Controller
    {
        private readonly SchoolContext _schoolContext;
        private readonly UserManager<User> _userManager;

        public SchoolTeachersController(SchoolContext schoolContext, UserManager<User> userManager)
        {
            _schoolContext = schoolContext;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var roles = await _userManager.GetRolesAsync(user);


            if (roles.Contains(SystemDefaultRoles.Administrator))
            {
                ViewData["schoolCount"] = 2;
                ViewData["schools"] = _schoolContext.Namespace;
                var schoolTeachers = await _schoolContext.SchoolTeacher.ToListAsync();
                return View(schoolTeachers);
            }


            var schoolCount = _schoolContext.SchoolTeacher.Where(x => x.TeacherId == user.Id).Count();
            var schoolId = _schoolContext.SchoolTeacher.Where(x => x.TeacherId == user.Id).Select(x => x.SchoolId);
            ViewData["schoolCount"] = schoolCount;
            ViewData["schools"] = _schoolContext.Namespace.Where(x => schoolId.Contains(x.Id));
            var schoolContext = await _schoolContext.SchoolTeacher.Where(x => schoolId.Contains(x.SchoolId)).ToListAsync();

            return View(schoolContext);
        }

        public async Task<IActionResult> SchoolChosenIndex(string id)
        {
            ViewData["school"] = await _schoolContext.Namespace.FindAsync(id);
            var schoolContext = await _schoolContext.SchoolTeacher.Where(x => x.SchoolId == id).Include(x => x.Teacher).ToListAsync();

            return View(schoolContext);
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolTeacher = await _schoolContext.SchoolTeacher
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolTeacher == null)
            {
                return NotFound();
            }

            return View(schoolTeacher);
        }

        public async Task<IActionResult> Create()
        {
            var teachers = await _userManager.GetUsersInRoleAsync(SystemDefaultRoles.Teacher);

            var viewModel = new SchoolTeacherViewModel
            {
                Schools = await _schoolContext.Namespace.ToListAsync(),
                Users = teachers.ToList(),
                SchoolNames = _schoolContext.Namespace.Select(x => x.Name).ToList(),
                UserNames = teachers.Select(x => x.UserName).ToList(),
                SchoolTeachers = await _schoolContext.SchoolTeacher.ToListAsync()
            };

            return View(new SchoolTeacherInputModel { ViewModel = viewModel });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SchoolTeacherInputModel model)
        {
            if (ModelState.IsValid)
            {
                var teacher = _userManager.Users.FirstOrDefault(x => x.UserName == model.Teacher);
                var school = _schoolContext.Namespace.FirstOrDefault(x => x.Name == model.School);
                var schoolTeachers = await _schoolContext.SchoolTeacher.ToListAsync();
                
                if (!_schoolContext.SchoolTeacher.Any(e => e.TeacherId == teacher.Id && e.SchoolId == school.Id))
                {
                    var schoolTeacher = new SchoolTeacher()
                    {
                        Id = Guid.NewGuid().ToString(),
                        TeacherId = teacher.Id,
                        School = school
                    };

                    _schoolContext.Add(schoolTeacher);
                    await _schoolContext.SaveChangesAsync();
                    return RedirectToAction("SchoolList", "Admin");
                }
            }
            return NotFound();
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolTeacher = await _schoolContext.SchoolTeacher
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolTeacher == null)
            {
                return NotFound();
            }

            return View(schoolTeacher);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var schoolTeacher = await _schoolContext.SchoolTeacher.FindAsync(id);
            _schoolContext.SchoolTeacher.Remove(schoolTeacher);
            await _schoolContext.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

    }
}
