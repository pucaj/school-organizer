﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Common.Security.Roles;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.Models.School;
using SzkolaXXII.Web.Policies;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.Controllers
{
    [Authorize(Policy = AuthorizationPolicies.Name.AtLeastTeacher)]
    public class SchoolClassesController : Controller
    {
        private readonly SchoolContext _context;
        private readonly UserManager<User> _userManager;

        public SchoolClassesController(SchoolContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            var roles = await _userManager.GetRolesAsync(user);


            if (roles.Contains(SystemDefaultRoles.Administrator))
            {
                ViewData["schoolCount"] = 2;
                ViewData["schools"] = _context.Namespace;
                var schoolClasses = await _context.SchoolClass.Include(s => s.Educator.Teacher).ToListAsync();
                return View(schoolClasses);
            }


            var schoolCount = _context.SchoolTeacher.Where(x => x.TeacherId == user.Id).Count();
            var schoolId = _context.SchoolTeacher.Where(x => x.TeacherId == user.Id).Select(x => x.SchoolId);
            ViewData["schoolCount"] = schoolCount;
            ViewData["schools"] = _context.Namespace.Where(x => schoolId.Contains(x.Id));
            var schoolContext = await _context.SchoolClass.Where(x => schoolId.Contains(x.SchoolId)).Include(s => s.Educator.Teacher).ToListAsync();

            return View(schoolContext);
        }

        public async Task<IActionResult> SchoolChosenIndex(string id)
        {
            ViewData["school"] = await _context.Namespace.FindAsync(id);
            var schoolContext = await _context.SchoolClass.Include(s => s.Educator.Teacher).Where(x => x.SchoolId == id).ToListAsync();
            return View(schoolContext);
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClass = await _context.SchoolClass
                .Include(s => s.Educator)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (schoolClass == null)
            {
                return NotFound();
            }

            return View(schoolClass);
        }

        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> Create()
        {
            List<SelectListItem> listOfTeachers = (from st in _context.SchoolTeacher
                                                   where !(
                                                      from sc in _context.SchoolClass
                                                      select sc.EducatorId
                                                    ).Contains(st.Id)
                                                   select new SelectListItem { Text = st.Id, Value = st.Teacher.UserName }).ToList();
            ViewData["EducatorCount"] = _context.SchoolTeacher.Count();
            ViewData["EducatorId"] = new SelectList(listOfTeachers, "Text", "Value");
            ViewData["School"] = new SelectList(_context.Namespace, "Id", "Name");
            ViewData["SchoolsCount"] = _context.Namespace.Count();

            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> Create([Bind("Id,Name,StartSchoolDate,EndSchoolDate,EducatorId,SchoolId")] SchoolClass schoolClassInput)
        {
            if (ModelState.IsValid && !_context.SchoolClass.Any(e => e.EducatorId == schoolClassInput.EducatorId))
            {
                var schoolClass = new SchoolClass
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = schoolClassInput.Name,
                    StartSchoolDate = schoolClassInput.StartSchoolDate,
                    EndSchoolDate = schoolClassInput.EndSchoolDate,
                    Educator = _context.SchoolTeacher.FirstOrDefault(x => x.Id == schoolClassInput.EducatorId),
                    EducatorId = schoolClassInput.EducatorId,
                    School = _context.Namespace.FirstOrDefault(x => x.Id == schoolClassInput.SchoolId),
                    SchoolId = schoolClassInput.SchoolId
                };

                _context.Add(schoolClass);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            var teachers = await _context.SchoolTeacher.Include(s => s.Teacher).ToListAsync();
            IEnumerable<SelectListItem> items = teachers.Select(teacher => new SelectListItem
            {
                Value = teacher.Id,
                Text = teacher.Teacher.Email
            });

            ViewData["EducatorCount"] = _context.SchoolTeacher.Count();
            ViewData["EducatorId"] = items;
            ViewData["SchoolsCount"] = _context.Namespace.Count();
            ViewData["School"] = new SelectList(_context.Namespace, "Id", "Name", schoolClassInput.SchoolId);

            return View(schoolClassInput);
        }

        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var schoolClass = await _context.SchoolClass.FindAsync(id);
            if (schoolClass == null)
            {
                return NotFound();
            }

            var teachers = await _context.SchoolTeacher.Include(s => s.Teacher).ToListAsync();
            IEnumerable<SelectListItem> items = teachers.Select(teacher => new SelectListItem
            {
                Value = teacher.Id,
                Text = teacher.Teacher.Email
            });

            ViewData["EducatorsCount"] = await _context.SchoolTeacher.CountAsync();
            ViewData["EducatorId"] = items;

            return View(schoolClass);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Name,StartSchoolDate,EndSchoolDate,EducatorId,SchoolId")] SchoolClass schoolClass)
        {
            if (id != schoolClass.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(schoolClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!SchoolClassExists(schoolClass.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ex;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            var teachers = await _context.SchoolTeacher.Include(s => s.Teacher).ToListAsync();
            IEnumerable<SelectListItem> items = teachers.Select(teacher => new SelectListItem
            {
                Value = teacher.Id,
                Text = teacher.Teacher.Email
            });

            ViewData["EducatorsCount"] = await _context.SchoolTeacher.CountAsync();
            ViewData["EducatorId"] = items;
            return View(schoolClass);
        }

        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var schoolClass = await _context.SchoolClass
                .Include(s => s.Educator.Teacher)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolClass == null)
            {
                return NotFound();
            }
            return View(schoolClass);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var schoolClass = await _context.SchoolClass.FindAsync(id);

            _context.SchoolClass.Remove(schoolClass);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> ShowStudents(string id)
        {
            if (_context.SchoolStudent.Count() <= 0)
            {
                return View(new List<SchoolStudentIndexViewModel>());
            }

            var viewModel = new List<SchoolStudentIndexViewModel>();

            foreach (var student in _context.SchoolStudent)
            {
                if (student.SchoolClassId == id)
                {
                    viewModel.Add(new SchoolStudentIndexViewModel
                    {
                        StudentId = student.Id,
                        Student = await _userManager.FindByIdAsync(student.StudentId),
                        SchoolClass = _context.SchoolClass.FirstOrDefault(n => n.Id == student.SchoolClassId),
                        SchoolClassId = student.SchoolClassId
                    });
                }
            }

            string educator = _context.SchoolClass.FirstOrDefault(n => n.Id == id).EducatorId;
            string educatorId = _context.SchoolTeacher.FirstOrDefault(n => n.Id == educator).TeacherId;
            string educatorName = _context.Users.FirstOrDefault(n => n.Id == educatorId).UserName;
            ViewData["EducatorName"] = educatorName;
            ViewData["EducatorId"] = educatorId;
            ViewData["ClassName"] = _context.SchoolClass.FirstOrDefault(n => n.Id == id).Name;
            return View(viewModel);
        }

        private bool SchoolClassExists(string id)
        {
            return _context.SchoolClass.Any(e => e.Id == id);
        }
    }
}
