﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Common.Security.Roles;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.InputModel;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.Controllers
{
    [Authorize(Policy = Policies.AuthorizationPolicies.Name.AtLeastTeacher)]
    public class AssignmentsController : Controller
    {
        private readonly SchoolContext _context;

        public AssignmentsController(SchoolContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.Assigment.ToListAsync());
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignment = await _context.Assigment
                .FirstOrDefaultAsync(m => m.Id == id);
            if (assignment == null)
            {
                return NotFound();
            }

            return View(assignment);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AssignmentInputModel model)
        {
            if (ModelState.IsValid)
            {

                var subject = _context.Subject.FirstOrDefault(x => x.Id == model.SubjectId);

                var assignment = new Assignment()
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = model.Title,
                    Description = model.Description,
                    Type = model.Type,
                    Weight = model.Weight,
                    Date = model.Date,
                    SubjectId = model.SubjectId,
                    Subject = subject,

                };
                _context.Add(assignment);
                await _context.SaveChangesAsync();

            }
            return RedirectToAction(nameof(Index));
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignment = await _context.Assigment.FindAsync(id);
            if (assignment == null)
            {
                return NotFound();
            }

            return View(assignment);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAssignment(AssignmentInputModel model)
        {
            if (ModelState.IsValid)
            {
                var subject = _context.Subject.FirstOrDefault(x => x.Id == model.SubjectId);

                var assignment = new Assignment()
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = model.Title,
                    Description = model.Description,
                    Date = model.Date,
                    SubjectId = model.SubjectId
                };

                _context.Add(assignment);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Title,Type,Weight,Date,SubjectId,Description")] Assignment assignment)
        {
            if (id != assignment.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(assignment);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!AssignmentExists(assignment.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ex;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            return View(assignment);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var assignment = await _context.Assigment
                .FirstOrDefaultAsync(m => m.Id == id);

            if (assignment == null)
            {
                return NotFound();
            }

            return View(assignment);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var assignment = await _context.Assigment.FindAsync(id);
            _context.Assigment.Remove(assignment);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool AssignmentExists(string id)
        {
            return _context.Assigment.Any(e => e.Id == id);
        }
    }
}
