﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Common.Const;
using SzkolaXXII.Common.Security.Roles;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.InputModel;
using SzkolaXXII.Web.Models.School;
using SzkolaXXII.Web.Policies;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.Controllers
{
    [Authorize(Roles = SystemDefaultRoles.Teacher)]
    public class TeacherPanelController : Controller
    {
        private readonly SchoolContext _context;
        private readonly UserManager<User> _userManager;

        public TeacherPanelController(SchoolContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            var userId = _userManager.GetUserId(User);

            if (userId == null)
            {
                return NotFound();
            }

            var schoolTeacher = _context.SchoolTeacher.FirstOrDefault(x => x.TeacherId == userId);

            if (schoolTeacher == null)
            {
                return NotFound();
            }

            return View();
        }

        public IActionResult Subjects()
        {
            var userId = _userManager.GetUserId(User);

            if (userId == null)
            {
                return NotFound();
            }

            var schoolTeacher = _context.SchoolTeacher.FirstOrDefault(x => x.TeacherId == userId);

            if (schoolTeacher == null)
            {
                return NotFound();
            }

            var subjectTeacher = _context.SubjectTeacher.FirstOrDefault(x => x.SchoolTeacher.Id == schoolTeacher.Id);

            if (subjectTeacher == null)
            {
                return View(null);
            }

            var subjects = _context.Subject.Where(x => _context.SubjectTeacher.Any(y => y.SchoolTeacherId == schoolTeacher.Id && x.Id == y.SubjectId));

            return View(subjects);
        }

        public IActionResult AddClass(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subject = _context.Subject.FirstOrDefault(x => x.Id == id);

            if (subject == null)
            {
                return NotFound();
            }

            var schoolClass = _context.SchoolClass.Where(x => x.SchoolId == subject.SchoolId).ToList();

            if (schoolClass == null)
            {
                return NotFound();
            }

            var assignSchoolClass = _context.SchoolClass.ToList().Where(x => _context.SubjectSchoolClass.Any(y => y.SchoolClass == x && y.Subject.Id == id && x.SchoolId == subject.SchoolId));

            if (assignSchoolClass == null)
            {
                return View(new AddClassInputModel
                {
                    Id = id,
                    List = schoolClass
                });
            }

            schoolClass = schoolClass.Except(assignSchoolClass).ToList();

            if (schoolClass == null)
            {
                return NotFound();
            }

            return base.View(new AddClassInputModel
            {
                Id = id,
                List = schoolClass.ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddClass(AddClassInputModel model)
        {
            var schoolClass = _context.SchoolClass.FirstOrDefault(x => model.ClassId == x.Id);
            var subject = _context.Subject.FirstOrDefault(x => model.Id == x.Id);

            _context.SubjectSchoolClass.Add(new SubjectSchoolClass
            {
                Id = Guid.NewGuid().ToString(),
                SchoolClass = schoolClass,
                Subject = subject
            });

            await _context.SaveChangesAsync();

            return Redirect("~/TeacherPanel/Class/" + model.Id);
        }

        public async Task<IActionResult> Subject(string id)
        {
            var subject = _context.Subject.FirstOrDefault(n => n.Id == id);
            if (subject == null)
            {
                return NotFound();
            }

            var vievModel = new SubjectShowViewModel() { Assignments = null, Subject = subject };

            return View(vievModel);
        }

        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastTeacher)]
        public async Task<IActionResult> Create(string schoolId)
        {
            var user = await _userManager.GetUserAsync(User);
            var roles = await _userManager.GetRolesAsync(user);

            if (schoolId == null && (!roles.Contains(SystemDefaultRoles.Administrator) &&
                !roles.Contains(SystemDefaultRoles.Moderator)))
            {
                var userId = _userManager.GetUserId(User);
                var schoolTeacher = _context.SchoolTeacher.FirstOrDefault(x => x.TeacherId == userId);

                if (schoolTeacher == null)
                {
                    return View("Error");
                }

                var school = _context.Namespace.FirstOrDefault(x => x.Id == schoolTeacher.SchoolId);

                if (school == null)
                {
                    return View("Error");
                }

                schoolId = schoolTeacher.SchoolId;
            }

            var viewModel = new SubjectViewModel
            {
                SchoolId = schoolId,
                Subjects = await _context.Subject.ToListAsync(),
            };

            return View(new SubjectInputModel { ViewModel = viewModel, SchoolId = schoolId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SubjectInputModel model)
        {
            if (ModelState.IsValid)
            {
                var school = _context.Namespace.FirstOrDefault(x => x.Id == model.SchoolId);

                var subject = new Subject()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = model.Name,
                    Description = model.Description,
                    School = school
                };

                _context.Add(subject);
                await _context.SaveChangesAsync();

                var user = await _userManager.GetUserAsync(User);
                var schoolTeacher = _context.SchoolTeacher.FirstOrDefault(x => x.TeacherId == user.Id);

                if (schoolTeacher == null)
                {
                    return RedirectToAction(nameof(Index));
                }

                var subjectTeacher = new SubjectTeacher()
                {
                    Id = Guid.NewGuid().ToString(),
                    SchoolTeacher = schoolTeacher,
                    Subject = subject
                };

                _context.Add(subjectTeacher);
                await _context.SaveChangesAsync();

                return RedirectToAction(nameof(Subjects));
            }

            return NotFound();
        }

        public async Task<IActionResult> OneSubject(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subject = await _context.Subject.FindAsync(id);
            if (subject == null)
            {
                return NotFound();
            }

            var subjectSchoolClasses = _context.SubjectSchoolClass
                .Where(x => x.SubjectId == id)
                .Select(x => x.SchoolClassId);

            var schoolClasses = _context.SchoolClass
                .Where(x =>
                    subjectSchoolClasses
                        .Any(y => y == x.Id));

            var events = _context.ClassCalendarEvent.Where(x => schoolClasses.Any(y => y.Id == x.ClassId));
            var assignments = _context.Assigment.Where(x => x.SubjectId == id);

            OneSubjectViewModel oneSubjectView = new OneSubjectViewModel
            {
                Id = id,
                Name = subject.Name,
                Classes = schoolClasses,
                Events = events,
                Assignments = assignments
            };

            return View(oneSubjectView);
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var subject = await _context.Subject.FindAsync(id);
            if (subject == null)
            {
                return NotFound();
            }

            SubjectInputModel subjectInputModel = new SubjectInputModel
            {
                Id = subject.Id,
                Name = subject.Name,
                Description = subject.Description
            };

            return View(subjectInputModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Name,Description")] Subject subject)
        {
            if (id != subject.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(subject);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubjectExists(subject.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return Redirect("~/TeacherPanel/OneSubject/" + subject.Id);
            }

            return View(subject);
        }

        private bool SubjectExists(string id)
        {
            return _context.Subject.Any(e => e.Id == id);
        }

        public IActionResult Class(string id)
        {
            var list = _context.SchoolClass.ToList().Where(x => _context.SubjectSchoolClass.Any(y => y.SchoolClass == x && y.Subject.Id == id));

            if (list.ToList() == null || list.ToList().Count < 1)
            {
                return View(new SubjectSchoolClassViewModel
                {
                    List = new List<SchoolClass>(),
                    Id = id
                });
            }

            return View(new SubjectSchoolClassViewModel
            {
                List = list.ToList(),
                Id = id
            });
        }

        public async Task<IActionResult> Remove(string subjectId, string classId)
        {
            var schoolClass = await _context.SubjectSchoolClass.FirstOrDefaultAsync(x => x.SchoolClass.Id == classId && x.Subject.Id == subjectId);

            _context.SubjectSchoolClass.Remove(schoolClass);
            await _context.SaveChangesAsync();

            return Redirect("~/TeacherPanel/Class/" + subjectId);
        }

        public IActionResult Calendar(string id, string subjectId)
        {
            var list = _context.SchoolClass.Where(x => _context.SubjectSchoolClass.Any(y => y.SchoolClass == x && y.Subject.Id == subjectId));

            if (id == null && list.ToList().Count > 0) id = list.ToList()[0].Id;

            CalendarEventViewModel calendarEventViewModel = new CalendarEventViewModel
            {
                Month = DateTime.Today.Month,
                Year = DateTime.Today.Year,
                Events = GetEventList(id)
            };

            TeacherCalendarEventViewModel teacherCalendarEventViewModel = new TeacherCalendarEventViewModel
            {
                CalendarEventViewModel = calendarEventViewModel,
                Id = id,
                SubjectId = subjectId,
                SchoolClasses = list.ToList()
            };

            if (list.ToList().Count > 0) teacherCalendarEventViewModel.Name = _context.SchoolClass.FirstOrDefault(x => x.Id == id).Name;
            else teacherCalendarEventViewModel.Name = "";

            return View(teacherCalendarEventViewModel);
        }

        public IActionResult Prev(int month, int year, string id, string subjectId)
        {
            CalendarEventViewModel calendarEventViewModel = new CalendarEventViewModel();
            calendarEventViewModel.Events = GetEventList(id);

            if (month == 1)
            {
                calendarEventViewModel.Year = year - 1;
                calendarEventViewModel.Month = 12;
            }
            else
            {
                calendarEventViewModel.Month = month - 1;
                calendarEventViewModel.Year = year;
            }

            var list = _context.SchoolClass.Where(x => _context.SubjectSchoolClass.Any(y => y.SchoolClass == x && y.Subject.Id == subjectId));

            if (id == null && list.ToList().Count > 0) id = list.ToList()[0].Id;

            TeacherCalendarEventViewModel teacherCalendarEventViewModel = new TeacherCalendarEventViewModel
            {
                CalendarEventViewModel = calendarEventViewModel,
                Id = id,
                SubjectId = subjectId,
                SchoolClasses = list.ToList(),
                Name = _context.SchoolClass.FirstOrDefault(x => x.Id == id).Name
            };

            return View("Calendar", teacherCalendarEventViewModel);
        }

        public IActionResult Next(int month, int year, string id, string subjectId)
        {
            CalendarEventViewModel calendarEventViewModel = new CalendarEventViewModel();
            calendarEventViewModel.Events = GetEventList(id);

            if (month == 12)
            {
                calendarEventViewModel.Year = year + 1;
                calendarEventViewModel.Month = 1;
            }
            else
            {
                calendarEventViewModel.Month = month + 1;
                calendarEventViewModel.Year = year;
            }

            var list = _context.SchoolClass.Where(x => _context.SubjectSchoolClass.Any(y => y.SchoolClass == x && y.Subject.Id == subjectId));

            if (id == null && list.ToList().Count > 0) id = list.ToList()[0].Id;

            TeacherCalendarEventViewModel teacherCalendarEventViewModel = new TeacherCalendarEventViewModel
            {
                CalendarEventViewModel = calendarEventViewModel,
                Id = id,
                SubjectId = subjectId,
                SchoolClasses = list.ToList(),
                Name = _context.SchoolClass.FirstOrDefault(x => x.Id == id).Name
            };

            return View("Calendar", teacherCalendarEventViewModel);
        }

        private List<OneEventViewModel> GetEventList(string id)
        {
            List<OneEventViewModel> userEvents = new List<OneEventViewModel>();

            foreach (ClassCalendarEvent classCalendarEvent in _context.ClassCalendarEvent.Where(x => x.ClassId == id))
            {
                OneEventViewModel oneEventViewModel = new OneEventViewModel
                {
                    Date = classCalendarEvent.Date,
                    Title = classCalendarEvent.Title,
                    Type = classCalendarEvent.Type
                };

                userEvents.Add(oneEventViewModel);
            }

            return userEvents;
        }

        public IActionResult CreateClassEvent(string id, string subjectId)
        {
            ClassCalendarInputModel classCalendarInputModel = new ClassCalendarInputModel();
            classCalendarInputModel.ClassId = id;
            classCalendarInputModel.SubjectId = subjectId;
            return View(classCalendarInputModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateClassEvent(ClassCalendarInputModel classCalendarInputModel)
        {
            if (ModelState.IsValid)
            {
                ClassCalendarEvent classCalendarEvent = new ClassCalendarEvent()
                {
                    Id = Guid.NewGuid().ToString(),
                    Type = "warning",
                    ClassId = classCalendarInputModel.ClassId,
                    Title = classCalendarInputModel.Title,
                    Date = classCalendarInputModel.Date
                };

                _context.ClassCalendarEvent.Add(classCalendarEvent);
                await _context.SaveChangesAsync();
                return Redirect("~/TeacherPanel/Calendar/" + classCalendarInputModel.ClassId + "?subjectId=" + classCalendarInputModel.SubjectId);
            }

            return View(classCalendarInputModel);
        }

        public async Task<IActionResult> ShowStudents(string id, string subjectId)
        {
            if (_context.SchoolStudent.Count() <= 0)
            {
                return View(new List<SchoolStudentIndexViewModel>());
            }

            var viewModel = new List<SchoolStudentIndexViewModel>();

            foreach (var student in _context.SchoolStudent)
            {
                if (student.SchoolClassId == id)
                {
                    viewModel.Add(new SchoolStudentIndexViewModel
                    {
                        StudentId = student.Id,
                        Student = await _userManager.FindByIdAsync(student.StudentId),
                        SchoolClass = _context.SchoolClass.FirstOrDefault(n => n.Id == student.SchoolClassId),
                        SchoolClassId = student.SchoolClassId
                    });
                }
            }

            string educator = _context.SchoolClass.FirstOrDefault(n => n.Id == id).EducatorId;
            string educatorId = _context.SchoolTeacher.FirstOrDefault(n => n.Id == educator).TeacherId;
            string educatorName = _context.Users.FirstOrDefault(n => n.Id == educatorId).UserName;
            ViewData["EducatorName"] = educatorName;
            ViewData["EducatorId"] = educatorId;
            ViewData["ClassName"] = _context.SchoolClass.FirstOrDefault(n => n.Id == id).Name;
            ViewData["SubjectId"] = subjectId;
            return View(viewModel);
        }

        public IActionResult Notes()
        {
            var userId = _userManager.GetUserId(User);
            var notes = _context.Notes.Where(x => x.TeacherId == userId);
            
            List<NotesViewModel> list = new List<NotesViewModel>();

            foreach(var item in notes)
            {
                list.Add(new NotesViewModel
                {
                    Notes = item,
                    User = _context.Users.FirstOrDefault(x=> x.Id == item.StudentId)
                }); 
            }

            if(list.Count>1)list.OrderBy(x => x.Notes.Time);

            return View(list);
        }

        public IActionResult AddNotes(string id)
        {
            AddNotesInputModel addNotesInputModel = new AddNotesInputModel
            {
                StudentId = id
            };

            return View(addNotesInputModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddNotes(AddNotesInputModel model)
        {
            if (ModelState.IsValid)
            {
                var userId = _userManager.GetUserId(User);

                _context.Notes.Add(new Notes
                {
                    Id = Guid.NewGuid().ToString(),
                    StudentId = model.StudentId,
                    TeacherId = userId,
                    Read = false,
                    Time = DateTime.Today,
                    Text = model.Text,
                    Title = model.Title
                });

                await _context.SaveChangesAsync();

                return Redirect("~/TeacherPanel/Notes/");
            }
            else return View(model);            
        }

        public IActionResult EditNotes(string id)
        {
            var result = _context.Notes.FirstOrDefault(x => x.Id == id);

            AddNotesInputModel addNotesInput = new AddNotesInputModel
            {
                Id = id,
                Text = result.Text,
                Title = result.Title
            };

            return View(addNotesInput);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditNotes(AddNotesInputModel model)
        {
            if (ModelState.IsValid)
            {
                var result = _context.Notes.FirstOrDefault(x => x.Id == model.Id);
                result.Text = model.Text;
                result.Title = model.Title;
                result.Read = false;

                _context.Notes.Update(result);

                await _context.SaveChangesAsync();

                return Redirect("~/TeacherPanel/Notes/");
            }
            else return View(model);
        }

        public async Task<IActionResult> RemoveNotes(string id)
        {
            var notes = await _context.Notes.FirstOrDefaultAsync(x => x.Id == id);

            _context.Notes.Remove(notes);
            await _context.SaveChangesAsync();

            return Redirect("~/TeacherPanel/Notes/");
        }

        public IActionResult ShowNotes(string id)
        {
            var notes = _context.Notes.FirstOrDefault(x => x.Id == id);

            NotesViewModel notesViewModel = new NotesViewModel()
            {
                Notes = notes,
                User = _context.Users.FirstOrDefault(x => x.Id == notes.StudentId)
            };
            
            return View(notesViewModel);
        }

        public IActionResult Timetable(string id, string subjectId)
        {
            var list = _context.SchoolClass.Where(x => _context.SubjectSchoolClass.Any(y => y.SchoolClass == x && y.Subject.Id == subjectId));
            
            if (id == null && list.ToList().Count > 0) id = list.ToList()[0].Id;

            var timetables = _context.Timetable.Where(x => x.SchoolClassId == id);

            List<ElementOnTimetableViewModel> lessons = new List<ElementOnTimetableViewModel>();
            
            foreach(var x in timetables)
            {
                lessons.Add(new ElementOnTimetableViewModel
                {
                    Subject = _context.Subject.FirstOrDefault(z => z.Id == x.SubjectId),
                    Timetable = x
                }
                ); 
            }

            TeacherTimetableViewModel timetableViewModel = new TeacherTimetableViewModel
            {
                Id = id,
                SubjectId = subjectId,
                SchoolClasses = list.ToList(),
                elementOnTimetableViewModel = lessons,
                Check = true
            };

            if (list.ToList().Count > 0) timetableViewModel.Name = _context.SchoolClass.FirstOrDefault(x => x.Id == id).Name;
            else timetableViewModel.Name = "";

            return View(timetableViewModel);
        }

        public IActionResult CreateTimeTableEvent(string id, string subjectId)
        {
            TeacherTimetableAddInputModels teacherTimetableAddInputModels = new TeacherTimetableAddInputModels
            {
                SchoolClassId = id,
                SubjectId = subjectId,
                StartList = TimetableConst.StartLesson(),
                EndList = TimetableConst.EndLesson(),
                Error = "",
                DaysOfWeek = TimetableConst.Days()
            };

            return View(teacherTimetableAddInputModels);
        }

        [HttpPost]
        public async Task<IActionResult> CreateTimeTableEvent(TeacherTimetableAddInputModels teacherTimetableAddInputModels)
        {
             teacherTimetableAddInputModels.DaysOfWeek = TimetableConst.Days();
             teacherTimetableAddInputModels.StartList = TimetableConst.StartLesson();
             teacherTimetableAddInputModels.EndList = TimetableConst.EndLesson();

             if (ModelState.IsValid)
             {
                 DateTime startTime = DateTime.Parse(teacherTimetableAddInputModels.StartTime);
                 DateTime endTime = DateTime.Parse(teacherTimetableAddInputModels.EndTime);

                 if(startTime>endTime)
                 {
                     teacherTimetableAddInputModels.Error = "Podana godzina rozpoczęcia jest późniejsza niż godzina zakończenia";
                     return View(teacherTimetableAddInputModels);
                 }


                var findTeacher = _context.SubjectTeacher.FirstOrDefault(x => x.SubjectId == teacherTimetableAddInputModels.SubjectId);
                var checkClass = _context.Timetable.FirstOrDefault(x => x.SchoolClassId == teacherTimetableAddInputModels.SchoolClassId && x.StartTime <= startTime && x.EndTime >= startTime && x.DayofWeek == teacherTimetableAddInputModels.DayOfWeek);
                var checkTeacher = _context.Timetable.FirstOrDefault(x => _context.SubjectTeacher.Any(y => x.SubjectId == y.SubjectId && y.SchoolTeacherId == findTeacher.SchoolTeacherId && x.StartTime <= startTime && x.EndTime >= startTime && x.DayofWeek == teacherTimetableAddInputModels.DayOfWeek));

                if (checkClass !=null || checkTeacher != null)
                {
                    teacherTimetableAddInputModels.Error = "Zajęcia się pokrywają. Wybierz inny termin";
                    return View(teacherTimetableAddInputModels);
                }
                else
                {
                    _context.Timetable.Add( new Timetable()
                    {
                        DayofWeek = teacherTimetableAddInputModels.DayOfWeek,
                        StartTime = startTime,
                        EndTime = endTime,
                        SchoolClassId = teacherTimetableAddInputModels.SchoolClassId,
                        SubjectId = teacherTimetableAddInputModels.SubjectId,
                        Classroom = teacherTimetableAddInputModels.Classroom,
                        Id = Guid.NewGuid().ToString(),
                    });

                    await _context.SaveChangesAsync();

                    return Redirect("~/TeacherPanel/Timetable/" + teacherTimetableAddInputModels.SchoolClassId + "?subjectId=" + teacherTimetableAddInputModels.SubjectId);
                }
                
             }
             else return View(teacherTimetableAddInputModels);
        }

        public IActionResult ShowLesson(string id)
        {
            var lesson = _context.Timetable.FirstOrDefault(x => x.Id == id);
            var subject = _context.Subject.FirstOrDefault(x => x.Id == lesson.SubjectId);
            var schoolTeacher = _context.SchoolTeacher.FirstOrDefault(x => _context.SubjectTeacher.Any(y => x.Id == y.SchoolTeacherId && y.SubjectId == lesson.SubjectId));
            var user = _context.Users.FirstOrDefault(x => x.Id == schoolTeacher.TeacherId);
            var schoolStudent = _context.SchoolClass.FirstOrDefault(x => x.Id == lesson.SchoolClassId);

            ElementOnTimetableViewModel elementOnTimetableViewModel = new ElementOnTimetableViewModel()
            {
                Timetable = lesson,
                Subject = subject,
                TeacherName = user.Email,
                ClassName = schoolStudent.Name
            };

            return View(elementOnTimetableViewModel);
        }
    }
}