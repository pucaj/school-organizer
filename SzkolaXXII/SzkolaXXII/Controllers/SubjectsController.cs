﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Common.Security.Roles;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.InputModel;
using SzkolaXXII.Web.Models.Experience;
using SzkolaXXII.Web.Policies;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.Models.School
{
    public class SubjectsController : Controller
    {
        private readonly SchoolContext _schoolContext;
        private readonly UserManager<User> _userManager;

        public SubjectsController(SchoolContext schoolContext, UserManager<User> userManager)
        {
            _schoolContext = schoolContext;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index(string namespaceId)
        {
            if (string.IsNullOrEmpty(namespaceId))
            {
                var schoolTeacher = _schoolContext.SchoolTeacher
                    .FirstOrDefault(x => x.TeacherId == _userManager.GetUserId(User));
            }
            return View(await _schoolContext.Subject.ToListAsync());
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subject = await _schoolContext.Subject
                .FirstOrDefaultAsync(m => m.Id == id);
            if (subject == null)
            {
                return NotFound();
            }

            return View(subject);
        }

        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastTeacher)]
        public async Task<IActionResult> Create(string schoolId)
        {
            var user = await _userManager.GetUserAsync(User);
            var roles = await _userManager.GetRolesAsync(user);

            if (schoolId == null && (!roles.Contains(SystemDefaultRoles.Administrator) &&
                !roles.Contains(SystemDefaultRoles.Moderator)))
            {
                var userId = _userManager.GetUserId(User);
                var schoolTeacher = _schoolContext.SchoolTeacher.FirstOrDefault(x => x.TeacherId == userId);

                if (schoolTeacher == null)
                {
                    return View("Error");
                }

                var school = _schoolContext.Namespace.FirstOrDefault(x => x.Id == schoolTeacher.SchoolId);

                if (school == null)
                {
                    return View("Error");
                }

                schoolId = schoolTeacher.SchoolId;
            }

            var viewModel = new SubjectViewModel
            {
                SchoolId = schoolId,
                Subjects = await _schoolContext.Subject.ToListAsync(),
            };

            return View(new SubjectInputModel { ViewModel = viewModel, SchoolId = schoolId });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(SubjectInputModel model)
        {
            if (ModelState.IsValid)
            {
                var school = _schoolContext.Namespace.FirstOrDefault(x => x.Id == model.SchoolId);

                var subject = new Subject()
                {
                    Id = Guid.NewGuid().ToString(),
                    Name = model.Name,
                    Description = model.Description,
                    School = school
                };

                _schoolContext.Add(subject);
                await _schoolContext.SaveChangesAsync();

                var user = await _userManager.GetUserAsync(User);
                var schoolTeacher = _schoolContext.SchoolTeacher.FirstOrDefault(x => x.TeacherId == user.Id);

                if (schoolTeacher == null)
                {
                    return RedirectToAction(nameof(Index));
                }

                var subjectTeacher = new SubjectTeacher()
                {
                    Id = Guid.NewGuid().ToString(),
                    SchoolTeacher = schoolTeacher,
                    Subject = subject
                };

                _schoolContext.Add(subjectTeacher);
                await _schoolContext.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var subject = await _schoolContext.Subject.FindAsync(id);
            if (subject == null)
            {
                return NotFound();
            }
            return View(subject);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Name,Descripion")] Subject subject)
        {
            if (id != subject.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _schoolContext.Update(subject);
                    await _schoolContext.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubjectExists(subject.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            return View(subject);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subject = await _schoolContext.Subject
                .FirstOrDefaultAsync(m => m.Id == id);
            if (subject == null)
            {
                return NotFound();
            }

            return View(subject);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var subject = await _schoolContext.Subject.FindAsync(id);

            _schoolContext.Subject.Remove(subject);
            await _schoolContext.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        [Authorize(Roles = SystemDefaultRoles.Teacher)]
        public IActionResult CreateAssignment(string id)
        {
            if (!string.IsNullOrEmpty(id))
            {
                var parm = new AssignmentInputModel() { SubjectId = id, Date = DateTime.Now };
                ViewData["SubId"] = id;
                return View(parm);
            }
            return NotFound();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateAssignment(AssignmentInputModel model)
        {
            if (ModelState.IsValid)
            {
                var subject = _schoolContext.Subject.FirstOrDefault(x => x.Id == model.SubjectId);

                var assignment = new Assignment()
                {
                    Id = Guid.NewGuid().ToString(),
                    Title = model.Title,
                    Description = model.Description,
                    Type = model.Type,
                    Weight = model.Weight,
                    Date = model.Date,
                    SubjectId = model.SubjectId
                };

                _schoolContext.Add(assignment);
                await _schoolContext.SaveChangesAsync();

                return RedirectToAction(nameof(Index));
            }

            return NotFound();
        }

        private bool SubjectExists(string id)
        {
            return _schoolContext.Subject.Any(e => e.Id == id);
        }

        public async Task<IActionResult> Remove(string subjectId, string classId)
        {
            var schoolClass = await _schoolContext.SubjectSchoolClass.FirstOrDefaultAsync(x => x.SchoolClass.Id == classId && x.Subject.Id == subjectId);

            _schoolContext.SubjectSchoolClass.Remove(schoolClass);
            await _schoolContext.SaveChangesAsync();

            return Redirect("~/Subjects/Details/" + subjectId);
        }

        public IActionResult Class(string id)
        {
            var list = _schoolContext.SchoolClass.ToList().Where(x => _schoolContext.SubjectSchoolClass.Any(y => y.SchoolClass == x && y.Subject.Id == id));

            if (list.ToList() == null || list.ToList().Count < 1)
            {
                return View(new SubjectSchoolClassViewModel
                {
                    List = new List<SchoolClass>(),
                    Id = id
                });
            }

            return View(new SubjectSchoolClassViewModel
            {
                List = list.ToList(),
                Id = id
            });
        }

        public IActionResult AddClass(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subject = _schoolContext.Subject.FirstOrDefault(x => x.Id == id);

            if (subject == null)
            {
                return NotFound();
            }

            var schoolClass = _schoolContext.SchoolClass.Where(x => x.SchoolId == subject.SchoolId).ToList();

            if (schoolClass == null)
            {
                return NotFound();
            }

            var assignSchoolClass = _schoolContext.SchoolClass.ToList().Where(x => _schoolContext.SubjectSchoolClass.Any(y => y.SchoolClass == x && y.Subject.Id == id && x.SchoolId == subject.SchoolId));

            if (assignSchoolClass == null)
            {
                return View(new AddClassInputModel
                {
                    Id = id,
                    List = schoolClass
                });
            }

            schoolClass = schoolClass.Except(assignSchoolClass).ToList();

            if (schoolClass == null)
            {
                return NotFound();
            }

            return base.View(new AddClassInputModel
            {
                Id = id,
                List = schoolClass.ToList()
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddClass(AddClassInputModel model)
        {
            var schoolClass = _schoolContext.SchoolClass.FirstOrDefault(x => model.ClassId == x.Id);
            var subject = _schoolContext.Subject.FirstOrDefault(x => model.Id == x.Id);

            _schoolContext.SubjectSchoolClass.Add(new SubjectSchoolClass
            {
                Id = Guid.NewGuid().ToString(),
                SchoolClass = schoolClass,
                Subject = subject
            });

            await _schoolContext.SaveChangesAsync();

            await SetUserSubjectLevel(subject, schoolClass);

            return Redirect("~/Subjects/Details/" + model.Id);
        }

        private async Task SetUserSubjectLevel(Subject subject, SchoolClass schoolClass)
        {
            foreach (var user in schoolClass.Students)
            {
                if (_schoolContext.UserSubjectLevel.Any(x => x.SubjectId == subject.Id && x.UserId == user.StudentId))
                {
                    continue;
                }
                try
                {
                    var userSubjectLevel = new UserSubjectLevel
                    {
                        Id = Guid.NewGuid().ToString(),
                        Level = 1,
                        Experience = 0,
                        UserId = user.StudentId,
                        SubjectId = subject.Id
                    };

                    _schoolContext.UserSubjectLevel.Add(userSubjectLevel);
                    await _schoolContext.SaveChangesAsync();
                }
                catch (Exception) { }
            }
        }

        public async Task<IActionResult> Subject(string id)
        {
            var subject = _schoolContext.Subject.FirstOrDefault(n => n.Id == id);
            if (subject == null)
            {
                return NotFound();
            }
            var assigmnets = _schoolContext.Assigment.Where(n => n.SubjectId == id).ToList();
            var vievModel = new SubjectShowViewModel() { Assignments = assigmnets, Subject = subject };

            return View(vievModel);
        }

        public async Task<IActionResult> StudentSubject()
        {
            var currentUser = await _userManager.GetUserAsync(User);
            var currStudent = _schoolContext.SchoolStudent.FirstOrDefault(n => n.StudentId == currentUser.Id);
            var subjectList = new List<Subject>();
            if (currStudent != null) {
                foreach (var schoolClasses in _schoolContext.SubjectSchoolClass)
                {
                    if (schoolClasses.SchoolClassId == currStudent.SchoolClassId)
                    {
                        subjectList.Add(_schoolContext.Subject.FirstOrDefault(n => n.Id == schoolClasses.SubjectId));
                    }
                }
            }
            ViewData["SubjectCount"] = subjectList.Count;
            return View(subjectList);
        }

        public async Task<IActionResult> SetStudentGrades(string assignmentId, string subjectId)
        {
            List<SetStudentGradeViewModel> studentsList = new List<SetStudentGradeViewModel>();
            var classList = _schoolContext.SubjectSchoolClass.Select(n => n).Where(n => n.SubjectId == subjectId);
            foreach (var singleClass in classList)
            {
                var listStudent = _schoolContext.SchoolStudent.Select(n => n).Where(n => n.SchoolClassId == singleClass.SchoolClassId).ToList();
                var grade = _schoolContext.SubjectGrade.FirstOrDefault(n => n.SubjectId == assignmentId);
                foreach (var singleStudent in listStudent)
                {
                    var studentGrade = _schoolContext.SubjectGrade.FirstOrDefault(n => n.StudentId == singleStudent.Id && n.SubjectId == assignmentId);
                    if (studentGrade == null)
                    {
                        grade = new SubjectGrade() { Grade = 0 };
                    }
                    else
                    {
                        grade = new SubjectGrade() { Grade = studentGrade.Grade };
                    }
                    studentsList.Add(new SetStudentGradeViewModel()
                    {
                        User = _schoolContext.Users.FirstOrDefault(n => n.Id == singleStudent.StudentId),
                        SubjectGrade = grade
                    });
                }
            }
            ViewData["AssignmentId"] = assignmentId;
            return View(studentsList);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddNewGradeForStudent()
        {
            string assignmentId = HttpContext.Request.Form["AssignmentId"].ToString();
            int grade = int.Parse(HttpContext.Request.Form["NewGrade"]);
            var userId = HttpContext.Request.Form["UserId"].ToString();
            var studentId = _schoolContext.SchoolStudent.FirstOrDefault(n => n.StudentId == userId).Id;
            var assignment = _schoolContext.Assigment.FirstOrDefault(n => n.Id == assignmentId);
            var currUserId = (await _userManager.GetUserAsync(User)).Id.ToString();
            var teacherId = _schoolContext.SchoolTeacher.FirstOrDefault(n => n.TeacherId == currUserId).Id;
            var studentGrade = _schoolContext.SubjectGrade.FirstOrDefault(n => n.StudentId == studentId && n.SubjectId == assignment.Id);

            if (studentGrade == null)
            {
                _schoolContext.SubjectGrade.Add(new SubjectGrade
                {
                    Id = Guid.NewGuid().ToString(),
                    Grade = grade,
                    SubjectId = assignment.Id,
                    StudentId = _schoolContext.SchoolStudent.FirstOrDefault(n => n.StudentId == userId).Id,
                    TeacherId = teacherId
                });
                await _schoolContext.SaveChangesAsync();

                return Redirect("~/Subjects/Subject/" + assignment.SubjectId);
            }

            studentGrade.Grade = grade;
            _schoolContext.Update(studentGrade);
            await _schoolContext.SaveChangesAsync();

            await AddExperienceForGrade(studentGrade, userId);

            return Redirect("~/Subjects/Subject/" + assignment.SubjectId);
        }

        public async Task AddExperienceForGrade(SubjectGrade subjectGrade, string userId)
        {
            var userSubjectLevel = 
                _schoolContext.UserSubjectLevel.FirstOrDefault(x => 
                    x.UserId == userId && 
                    x.SubjectId == subjectGrade.SubjectId);

            if (userSubjectLevel == null)
            {

                userSubjectLevel = new UserSubjectLevel
                {
                    Id = Guid.NewGuid().ToString(),
                    Level = 1,
                    Experience = 0,
                    UserId = userId,
                    SubjectId = subjectGrade.SubjectId
                };
            }

            userSubjectLevel.Experience += (int)(subjectGrade.Grade * subjectGrade.Weight * UserSubjectLevel.GradeConst);

            var level = UserSubjectLevel.CalculateLevel(userSubjectLevel.Experience);
            if (level > userSubjectLevel.Level)
            {
                userSubjectLevel.Level = level;
            }

            _schoolContext.Update(userSubjectLevel);
            await _schoolContext.SaveChangesAsync();
        }

        public async Task<IActionResult> StudentGrades(string subjectId)
        {
            var currUser = await _userManager.GetUserAsync(User);
            var currStudent = _schoolContext.SchoolStudent.FirstOrDefault(n => n.StudentId == currUser.Id);
            var assignmentList = _schoolContext.Assigment.Select(n => n).Where(n => n.SubjectId == subjectId);
            List<StudentGradesViewModel> gradesList = new List<StudentGradesViewModel>();
            foreach (var singleAsignment in assignmentList)
            {
                var singleSubjectGrade = _schoolContext.SubjectGrade.FirstOrDefault(n => n.SubjectId == singleAsignment.Id && n.StudentId == currStudent.Id);
                if (singleSubjectGrade != null)
                {
                    gradesList.Add(new StudentGradesViewModel
                    {
                        AssignmentName = singleAsignment.Title,
                        AssignmentType = singleAsignment.Type,
                        Grade = singleSubjectGrade.Grade
                    });
                }
            }
            return View(gradesList);
        }
    }
}

