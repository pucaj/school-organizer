﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.Policies;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.Models.School
{
    public class SchoolStudentsController : Controller
    {
        private readonly SchoolContext _context;
        private readonly UserManager<User> _userManager;

        public SchoolStudentsController(SchoolContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            if (_context.SchoolStudent.Count() <= 0)
            {
                return View(null);
            }

            var viewModel = new List<SchoolStudentIndexViewModel>();

            foreach (var student in _context.SchoolStudent)
            {
                viewModel.Add(new SchoolStudentIndexViewModel
                {
                    StudentId = student.Id,
                    Student = await _userManager.FindByIdAsync(student.StudentId),
                    SchoolClass = _context.SchoolClass.FirstOrDefault(n => n.Id == student.SchoolClassId),
                    SchoolClassId = student.SchoolClassId
                });
            }
            return View(viewModel);
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var schoolStudent = _context.SchoolStudent
                            .Include(s => s.SchoolClass)
                            .Include(s => s.Student)
                            .Include(s => s.School).FirstOrDefault(n => n.Id == id);
            if (schoolStudent == null)
            {
                return NotFound();
            }
            return View(schoolStudent);
        }

        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public IActionResult Create()
        {
            List<SelectListItem> listOfStudents = (from ss in _context.SchoolStudent
                                           where ss.SchoolClassId == null
                                           select new SelectListItem { Text = ss.Student.Id, Value = ss.Student.UserName }).ToList();
            ViewData["StudentId"] = new SelectList(listOfStudents, "Text", "Value");
            ViewData["SchoolClass"] = new SelectList(_context.SchoolClass, "Id", "Name");
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> Create([Bind("Id,StudentId,SchoolClass")] AddStudentViewModel schoolStudent)
        {
            if (ModelState.IsValid)
            {
                var schoolClass = _context.SchoolClass.Find(schoolStudent.SchoolClass);
                if (schoolClass == null)
                {
                    return NotFound();
                }

                try
                {
                    var studentUser = _userManager.Users.FirstOrDefault(n => n.Id == schoolStudent.StudentId);
                    
                    var newStudent = _context.SchoolStudent.FirstOrDefault(n => n.StudentId == studentUser.Id);
                    newStudent.SchoolClassId = schoolClass.Id;

                    _context.Update(newStudent);

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw new Exception("Błąd przy edytowaniu", ex);
                }
                return RedirectToAction("ShowStudents", "SchoolClasses", new
                {
                    id = schoolClass.Id
                });
            }
            ViewData["StudentId"] = new SelectList(_userManager.Users, "Id", "UserName");
            ViewData["SchoolClass"] = new SelectList(_context.SchoolClass, "Id", "Name");
            return View();
        }

        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var schoolStudent = await _context.SchoolStudent.FindAsync(id);
            if (schoolStudent == null)
            {
                return NotFound();
            }
            ViewData["StudentId"] = new SelectList(_userManager.Users, "Id", "UserName");
            ViewData["SchoolClass"] = new SelectList(_context.SchoolClass, "Id", "Name");
            return View(schoolStudent);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> Edit(string id, [Bind("Id,StudentId,SchoolClass")] AddStudentViewModel schoolStudent)
        {
            if (id != schoolStudent.Id)
            {
                return NotFound();
            }
            if (ModelState.IsValid)
            {
                SchoolClass schoolClass;
                try
                {
                    schoolClass = _context.SchoolClass.Find(schoolStudent.SchoolClass);
                    var newStudent = _context.SchoolStudent.FirstOrDefault(n => n.Id == schoolStudent.Id);
                    newStudent.Id = schoolStudent.Id;
                    newStudent.StudentId = schoolStudent.StudentId;
                    newStudent.SchoolClassId = schoolClass.Id;
                    newStudent.SchoolClass = schoolClass;

                    _context.Update(newStudent);

                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    throw new Exception("Błąd przy edytowaniu", ex);
                }
                return RedirectToAction("ShowStudents", "SchoolClasses", new
                {
                    id = schoolClass.Id
                });
            }
            ViewData["StudentId"] = new SelectList(_context.Set<User>(), "Id", "Id", schoolStudent.StudentId);
            ViewData["SchoolClass"] = new SelectList(_context.Set<SchoolClass>(), "Id", "Name");
            return View(schoolStudent);
        }

        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }
            var schoolStudent = await _context.SchoolStudent
                .Include(s => s.SchoolClass)
                .Include(s => s.Student)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (schoolStudent == null)
            {
                return NotFound();
            }
            return View(schoolStudent);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastModerator)]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var schoolStudent = await _context.SchoolStudent.FindAsync(id);
            if (schoolStudent == null)
            {
                return NotFound();
            }

            _context.SchoolStudent.Remove(schoolStudent);
            await _context.SaveChangesAsync();
            return RedirectToAction("ShowStudents", "SchoolClasses", new
            {
                id = schoolStudent.SchoolClassId
            });
        }

        private bool SchoolStudentExists(string id)
        {
            return _context.SchoolStudent.Any(e => e.Id == id);
        }
    }
}
