﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Razor.TagHelpers;
using Microsoft.AspNetCore.Routing;
using SzkolaXXII.Web.ViewModel;


namespace SzkolaXXII.Web.Controllers
{
    [HtmlTargetElement("timetable", TagStructure = TagStructure.NormalOrSelfClosing)]
    public class TimetableTagHelper : TagHelper
    {
        public enum Headline { Godzina, Poniedziałek, Wtorek, Środa, Czwartek, Piątek, Sobota, Niedziela };
        public string[] DaysOfWeek = new string[] { "Poniedziałek", "Wtorek", "Środa", "Czwartek", "Piątek", "Sobota", "Niedziela", "check" };
        public string[] Hours = new string[] { "08:15 - 09:00", "09:15 - 10:00", "10:15 - 11:00", "11:15 - 12:00", "12:15 - 13:00", "13:15 - 14:00", "14:15 - 15:00", "15:05 - 15:50", "16:00 - 16:45", "16:50 - 17:35", "17:40 - 18:25", "18:30 - 19:15", "19:20 - 20:05", "20:10 - 20:55" };
        public List<ElementOnTimetableViewModel> Events { get; set; }
        public bool CheckController { get; set; }
        public object ControllerContext { get; private set; }

        public int[] Check = new int[8];
        public ElementOnTimetableViewModel Event;

        public override void Process(TagHelperContext context, TagHelperOutput output)
        {
            output.TagName = "section";
            output.Attributes.Add("class", "timetable");
            output.Content.SetHtmlContent(GetHtml());
            output.TagMode = TagMode.StartTagAndEndTag;
        }

        private string GetHtml()
        {
            var html = new XDocument(
                new XElement("div",
                    new XAttribute("class", "container-fluid"),
                        new XElement("table",
                            new XAttribute("class", "table table-bordered"),
                                new XElement("thead",
                                    new XAttribute("class", "thead-dark"),
                                        GetHeadHtml()
                                            ),
                                        new XElement("tbody",
                                            GetBodyHtml()
                                     )   
                                )
                            )   
                        );
                                                    
            return html.ToString();

            IEnumerable<XElement> GetHeadHtml()
            {
                yield return
                    new XElement("tr",
                        Enum.GetValues(typeof(Headline)).Cast<Headline>().Select(d =>
                            new XElement("th",
                                new XAttribute("scope", "col"),
                                   new XElement("h5",
                                        new XAttribute("class", "col-md-12 text-center"),
                                                d.ToString()
                                        )
                                   )
                            )
                        );
            }

            IEnumerable<XElement> GetBodyHtml()
            {

                for(int i = 0; i< Hours.Count(); i++)
                {
                    yield return
                        new XElement("tr",
                             new XAttribute("align", "center"),
                                new XElement("th",
                                    new XAttribute("class", "bg-light col-md-12"),
                                       new XElement("h6",
                                                 Hours[i]
                                            )
                                       ),
                                    GetBodyLessonHtml(i)
                                );
                }
                    
            }

            IEnumerable<XElement> GetBodyLessonHtml(int hours)
            {
                for(int j=0; j< DaysOfWeek.Count(); j++)
                {
                    Event = Events.FirstOrDefault(x => x.Timetable.DayofWeek == DaysOfWeek[j] && Hours[hours].Contains(x.Timetable.StartTime.ToString("HH:mm")));
                    
                    if (Check[j] == 0 && Event != null)
                    {
                        int size = 1;

                            XElement root = new XElement("td",
                                new XAttribute("class", "align-middle bg-info text-white"),
                                   new XElement("h6",
                                        new XAttribute("class", "col-md-12 text-center"),
                                            Event.Subject.Name  
                                        ),
                                   new XElement("small",
                                        new XAttribute("class", "col-md-12 text-center"),
                                            Event.Timetable.Classroom
                                        )
                                   );

                        for (int z = hours; z < Hours.Count(); z++)
                            if (Hours[z].Contains(Event.Timetable.EndTime.ToString("HH:mm")))
                            {
                                size = z - hours + 1;
                                break;
                            }

                        root.SetAttributeValue("rowspan", size);
                        root.SetAttributeValue("style", "cursor: pointer");

                        if(CheckController) root.SetAttributeValue("onclick", "parent.location='/" + "TeacherPanel" + "/ShowLesson/" + Event.Timetable.Id + "'");
                        else root.SetAttributeValue("onclick", "parent.location='/" + "Timetable" + "/ShowLesson/" + Event.Timetable.Id + "'");

                        Check[j] = size-1;

                        yield return root;

                    }
                    else if (Check[j] == 0)
                    {
                        XElement root =
                            new XElement("td",
                                new XAttribute("class", "text-center "),
                                   new XElement("h6",
                                        new XAttribute("class", "col-md-12 text-center"),
                                            string.Empty
                                        )
                                   );
                        if (j == 7) root.SetAttributeValue("style", "display:none");

                        yield return root;
                    }
                    else Check[j]--;
                }
            }
        }
    }
}