﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.InputModel;
using SzkolaXXII.Web.Models.School;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.Controllers
{
    public class CalendarEventController : Controller
    {
        private readonly SchoolContext _context;
        private readonly UserManager<User> _userManager;

        public CalendarEventController(SchoolContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            CalendarEventViewModel calendarEventViewModel = new CalendarEventViewModel
            {
                Month = DateTime.Today.Month,
                Year = DateTime.Today.Year,
                Events = GetEventList()
            };

            return View(calendarEventViewModel);
        }

        public IActionResult Prev(int month, int year)
        {
            CalendarEventViewModel calendarEventViewModel = new CalendarEventViewModel();
            calendarEventViewModel.Events = GetEventList();

            if (month == 1)
            {
                calendarEventViewModel.Year = year - 1;
                calendarEventViewModel.Month = 12;
            }
            else
            {
                calendarEventViewModel.Month = month - 1;
                calendarEventViewModel.Year = year;
            }

            return View("Index", calendarEventViewModel);
        }

        public IActionResult Next(int month, int year)
        {
            CalendarEventViewModel calendarEventViewModel = new CalendarEventViewModel();
            calendarEventViewModel.Events = GetEventList();

            if (month == 12)
            {
                calendarEventViewModel.Year = year + 1;
                calendarEventViewModel.Month = 1;
            }
            else
            {
                calendarEventViewModel.Month = month + 1;
                calendarEventViewModel.Year = year;
            }
            return View("Index", calendarEventViewModel);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Title,Date,Type")] CalendarEvent @calendarEvent)
        {
            if (ModelState.IsValid)
            {
                @calendarEvent.Id = Guid.NewGuid().ToString();
                @calendarEvent.Type = "info";
                @calendarEvent.UserId = _userManager.GetUserId(User);
                _context.Add(@calendarEvent);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(@calendarEvent);
        }

        public IActionResult CreateClass(string id)
        {
            ClassCalendarInputModel classCalendarInputModel = new ClassCalendarInputModel();
            classCalendarInputModel.ClassId = id;

            return View(classCalendarInputModel);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> CreateClass([Bind("Id,Title,Date,Type,ClassId")] ClassCalendarEvent @classCalendarEvent)
        {
            if (ModelState.IsValid)
            {
                @classCalendarEvent.Id = Guid.NewGuid().ToString();
                @classCalendarEvent.Type = "warning";
                _context.ClassCalendarEvent.Add(@classCalendarEvent);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(@classCalendarEvent);
        }

        private List<OneEventViewModel> GetEventList()
        {
            List<OneEventViewModel> userEvents = new List<OneEventViewModel>();

            foreach (CalendarEvent calendarEvent in _context.CalendarEvent.ToList<CalendarEvent>().Where(x => x.UserId == _userManager.GetUserId(User)))
            {
                OneEventViewModel oneEventViewModel = new OneEventViewModel
                {
                    Date = calendarEvent.Date,
                    Title = calendarEvent.Title,
                    Type = calendarEvent.Type
                };

                userEvents.Add(oneEventViewModel);
            }

            foreach (ClassCalendarEvent classCalendarEvent in _context.ClassCalendarEvent.ToList<ClassCalendarEvent>().Where(x => _context.SchoolStudent.ToList().Any(y => x.ClassId == y.SchoolClassId && y.StudentId == _userManager.GetUserId(User))))
            {
                OneEventViewModel oneEventViewModel = new OneEventViewModel
                {
                    Date = classCalendarEvent.Date,
                    Title = classCalendarEvent.Title,
                    Type = classCalendarEvent.Type
                };

                userEvents.Add(oneEventViewModel);
            }

            return userEvents;
        }
    }
}
