﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SzkolaXXII.Common.Security.Roles;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.Controllers
{
    public class TimetableController : Controller
    {
        private readonly SchoolContext _schoolContext;
        private readonly UserManager<User> _userManager;

        public TimetableController(SchoolContext schoolContext, UserManager<User> userManager)
        {
            _schoolContext = schoolContext;
            _userManager = userManager;
        }

        [Authorize(Roles = SystemDefaultRoles.Student)]
        public IActionResult Index()
        {
            string userId = _userManager.GetUserId(User);
            var schoolClass = _schoolContext.SchoolClass.FirstOrDefault(x => _schoolContext.SchoolStudent.Any(y => x.Id == y.SchoolClassId && y.StudentId == userId));
            string name;
            List<ElementOnTimetableViewModel> lessons = new List<ElementOnTimetableViewModel>();

            if (schoolClass != null)
            {
                name = schoolClass.Name;

                var timetables = _schoolContext.Timetable.Where(x => x.SchoolClassId == schoolClass.Id);

                foreach (var x in timetables)
                {
                    lessons.Add(new ElementOnTimetableViewModel
                    {
                        Subject = _schoolContext.Subject.FirstOrDefault(z => z.Id == x.SubjectId),
                        Timetable = x
                    }
                    );
                }
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Plan jest pusty.");
                name = "Brak";
            }

            TeacherTimetableViewModel timetableViewModel = new TeacherTimetableViewModel
            {
                Name = name,
                elementOnTimetableViewModel = lessons
            };
             
            return View(timetableViewModel);
        }

        [Authorize(Roles = SystemDefaultRoles.Teacher)]
        public IActionResult TeacherIndex()
        {
            string userId = _userManager.GetUserId(User);

            var schoolTeacher = _schoolContext.SchoolTeacher.FirstOrDefault(x => x.TeacherId == userId);

            var timetables = _schoolContext.Timetable.Where(x => _schoolContext.SubjectTeacher.Any(y => x.SubjectId == y.SubjectId && y.SchoolTeacherId == schoolTeacher.Id));

            List<ElementOnTimetableViewModel> lessons = new List<ElementOnTimetableViewModel>();

            foreach (var x in timetables)
            {
                lessons.Add(new ElementOnTimetableViewModel
                {
                    Subject = _schoolContext.Subject.FirstOrDefault(z => z.Id == x.SubjectId),
                    Timetable = x
                }
                );
            }

            return View(lessons);
        }

        public IActionResult ShowLesson(string id)
        {
            var lesson = _schoolContext.Timetable.FirstOrDefault(x => x.Id == id);
            var subject = _schoolContext.Subject.FirstOrDefault(x => x.Id == lesson.SubjectId);
            var schoolTeacher = _schoolContext.SchoolTeacher.FirstOrDefault(x => _schoolContext.SubjectTeacher.Any(y => x.Id == y.SchoolTeacherId && y.SubjectId == lesson.SubjectId));
            var user = _schoolContext.Users.FirstOrDefault(x => x.Id == schoolTeacher.TeacherId);
            var schoolStudent = _schoolContext.SchoolClass.FirstOrDefault(x => x.Id == lesson.SchoolClassId);

            ElementOnTimetableViewModel elementOnTimetableViewModel = new ElementOnTimetableViewModel()
            {
                Timetable = lesson,
                Subject = subject,
                TeacherName = user.Email,
                ClassName = schoolStudent.Name
            };

            return View(elementOnTimetableViewModel);
        }
    }
}