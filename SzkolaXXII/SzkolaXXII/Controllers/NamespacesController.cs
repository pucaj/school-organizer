﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.Controllers
{
    public class NamespacesController : Controller
    {
        private readonly SchoolContext _context;

        public NamespacesController(SchoolContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            return View(await _context.Namespace.ToListAsync());
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @namespace = await _context.Namespace
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@namespace == null)
            {
                return NotFound();
            }

            return View(@namespace);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Address,Name")] Namespace @namespace)
        {
            if (ModelState.IsValid)
            {
                @namespace.Id = Guid.NewGuid().ToString();
                _context.Add(@namespace);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(@namespace);
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @namespace = await _context.Namespace.FindAsync(id);
            if (@namespace == null)
            {
                return NotFound();
            }
            return View(@namespace);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Address,Name")] Namespace @namespace)
        {
            if (id != @namespace.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(@namespace);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!NamespaceExists(@namespace.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ex;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(@namespace);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var @namespace = await _context.Namespace
                .FirstOrDefaultAsync(m => m.Id == id);
            if (@namespace == null)
            {
                return NotFound();
            }

            return View(@namespace);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var @namespace = await _context.Namespace.FindAsync(id);

            _context.Namespace.Remove(@namespace);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        private bool NamespaceExists(string id)
        {
            return _context.Namespace.Any(e => e.Id == id);
        }
    }
}
