﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.Models
{
    public class SubjectGradesController : Controller
    {
        private readonly SchoolContext _context;
        private readonly UserManager<User> _userManager;

        public SubjectGradesController(SchoolContext context, UserManager<User> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        public async Task<IActionResult> Index()
        {
            var schoolContext = _context.SubjectGrade.Include(s => s.Subject);
            return View(await schoolContext.ToListAsync());
        }

        public async Task<IActionResult> Details(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subjectGrade = await _context.SubjectGrade
                .Include(s => s.Subject)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (subjectGrade == null)
            {
                return NotFound();
            }

            return View(subjectGrade);
        }

        public IActionResult Create(string id = null)
        {
            SubjectGrade model = null;


            if (id == null)
            {
                ViewData["SubjectId"] = new SelectList(_context.Assigment, "Id", "Title");
            }
            else
            {
                var assignment = _context.Assigment.FirstOrDefault(x => x.Id == id);
                var classes = _context.SubjectSchoolClass
                    .Where(x => x.SubjectId == assignment.SubjectId)
                    .Select(x => x.SchoolClass);

                var students = _context.SchoolStudent
                    .Where(x => classes.Any(y => y.Id == x.SchoolClassId));

                var studentEmails = _userManager.Users
                    .Where(x => students.Any(y => y.StudentId == x.Id))
                    .Select(x => x.Email)
                    .ToList();

                var studentsIds = students.Select(x => x.Id).ToList();

                var emailsAndIds = studentsIds
                    .Zip(studentEmails)
                    .Select(x => new SelectModel
                    {
                        Id = x.First,
                        Email = x.Second
                    })
                    .ToList();

                ViewData["StudentId"] = new SelectList(emailsAndIds, "Id", "Email");

                model = new SubjectGrade { SubjectId = id };
            }

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Grade,Weight,StudentId,SubjectId")] SubjectGrade subjectGrade)
        {
            subjectGrade.Id = Guid.NewGuid().ToString();

            if (ModelState.IsValid && ValidateGradeModel(subjectGrade))
            {
                _context.Add(subjectGrade);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }

            if (subjectGrade == null)
            {
                ViewData["SubjectId"] = new SelectList(_context.Assigment, "Id", "Title", subjectGrade.SubjectId);
            }

            return View(subjectGrade);
        }

        public bool ValidateGradeModel(SubjectGrade grade)
        {
            if(grade.Grade < 0 || grade.Grade > 6)
            {
                return false;
            }

            if(grade.Weight < 0)
            {
                return false;
            }

            return true;
        }

        public async Task<IActionResult> Edit(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subjectGrade = await _context.SubjectGrade.FindAsync(id);
            if (subjectGrade == null)
            {
                return NotFound();
            }

            ViewData["SubjectId"] = new SelectList(_context.Assigment, "Id", "Id", subjectGrade.SubjectId);
            return View(subjectGrade);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(string id, [Bind("Id,Grade,Weight,SubjectId")] SubjectGrade subjectGrade)
        {
            if (id != subjectGrade.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(subjectGrade);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    if (!SubjectGradeExists(subjectGrade.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw ex;
                    }
                }

                return RedirectToAction(nameof(Index));
            }

            ViewData["SubjectId"] = new SelectList(_context.Assigment, "Id", "Id", subjectGrade.SubjectId);
            return View(subjectGrade);
        }

        public async Task<IActionResult> Delete(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var subjectGrade = await _context.SubjectGrade
                .Include(s => s.Subject)
                .FirstOrDefaultAsync(m => m.Id == id);

            if (subjectGrade == null)
            {
                return NotFound();
            }

            return View(subjectGrade);
        }

        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        {
            var subjectGrade = await _context.SubjectGrade.FindAsync(id);

            _context.SubjectGrade.Remove(subjectGrade);
            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }

        private bool SubjectGradeExists(string id)
        {
            return _context.SubjectGrade.Any(e => e.Id == id);
        }

        private class SelectModel
        {
            public string Id { get; set; }
            public string Email { get; set; }
        }
    }
}
