﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Common.Security.Roles;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.InputModel;
using SzkolaXXII.Web.Models.School;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.Controllers
{
    [Authorize(Roles = SystemDefaultRoles.Administrator)]
    public class AdminController : Controller
    {
        private readonly UserManager<User> _userManager;
        private readonly SchoolContext _context;

        public AdminController(UserManager<User> userManager, SchoolContext usersContext)
        {
            _userManager = userManager;
            _context = usersContext;
        }

        public async Task<IActionResult> Index()
        {
            return RedirectToAction("Roles");
        }

        public async Task<IActionResult> Roles()
        {
            var model = new List<AdminUserRolesViewModel>();

            foreach (var user in _userManager.Users)
            {
                var roles = (await _userManager.GetRolesAsync(user)).ToList();

                model.Add(new AdminUserRolesViewModel
                {
                    UserId = user.Id,
                    Name = user.UserName,
                    Email = user.Email,
                    Schools = null,
                    Roles = roles
                });
            }

            return View(model);
        }

        public async Task<IActionResult> AddUserRole(string id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var user = _userManager.Users.FirstOrDefault(x => x.Id == id);
            if (user == null)
            {
                return NotFound();
            }

            var allRoles = _context.Roles.Select(x => x.Name).ToList();
            var userRoles = (await _userManager.GetRolesAsync(user)).ToList();

            var schools = _context.Namespace.ToList();
            var schoolDictionary = new Dictionary<string, string>();

            schools.ForEach(x =>
            {
                schoolDictionary.Add(x.Name, x.Id);
            });

            var viewModel = new AdminUserRolesViewModel
            {
                UserId = id,
                Name = user.UserName,
                Email = user.Email,
                Roles = allRoles.Where(x => !userRoles.Contains(x)).ToList(),
                Schools = schoolDictionary
            };

            return View(new UserRoleInputModel
            {
                ViewModel = viewModel
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AddUserRole(UserRoleInputModel model)
        {
            if (model.Role == null || model.ViewModel.UserId == null)
            {
                return NotFound("Wprowadzone dane są niepoprawne.");
            }

            var user = _userManager.Users.FirstOrDefault(x => x.Id == model.ViewModel.UserId);
            if (user == null)
            {
                return NotFound("Nie znaleziono takiego użytkownika.");
            }

            try
            {
                await _userManager.AddToRoleAsync(user, model.Role);
                await _userManager.UpdateAsync(user);

                await CreateSubTableDataForRole(user, model);

                return RedirectToAction("Roles");
            }
            catch (Exception ex)
            {
                return NotFound(ex.Message);
            }
        }

        public async Task<IActionResult> DeleteUserRole(string id)
        {
            var user = _userManager.Users.FirstOrDefault(x => x.Id == id);

            if (user == null)
            {
                return NotFound();
            }

            var roles = await _userManager.GetRolesAsync(user);

            if (roles.Count <= 0)
            {
                return NotFound("Brak ról przypisanych do użytkownika.");
            }

            var schoolId = string.Empty;

            if (roles.Contains(SystemDefaultRoles.Student))
            {
                schoolId = _context.SchoolStudent.FirstOrDefault(x => x.StudentId == user.Id)?.SchoolId;

            }
            else if (roles.Contains(SystemDefaultRoles.Teacher))
            {
                schoolId = _context.SchoolTeacher.FirstOrDefault(x => x.TeacherId == user.Id)?.SchoolId;
            }

            var schools = new Dictionary<string, string>();

            _context.Namespace
                .Where(x => x.Id == schoolId)
                .ToList()
                .ForEach(x => schools.Add(x.Name, x.Id));

            var model = new UserRoleInputModel
            {
                ViewModel = new AdminUserRolesViewModel
                {
                    UserId = user.Id,
                    Name = user.UserName,
                    Email = user.Email,
                    Roles = roles.ToList(),
                    Schools = schools
                },
                SchoolId = schoolId
            };

            ViewData["ErrorMessage"] = "";
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteUserRole(UserRoleInputModel model, string role)
        {
            if (string.IsNullOrEmpty(role) || model == null)
            {
                return NotFound();
            }

            if (_context.Roles.FirstOrDefault(x => x.Name == role) == null)
            {
                return NotFound();
            }

            var user = _userManager.Users.FirstOrDefault(x => x.Id == model.ViewModel.UserId);
            if (user == null)
            {
                return NotFound();
            }

            try
            {
                await _userManager.RemoveFromRoleAsync(user, role);
                await _userManager.UpdateAsync(user);

                await DeleteSubTableDataForUser(user, role, model.SchoolId);

                return RedirectToAction("Roles");
            }
            catch (Exception ex)
            {
                ViewData["ErrorMessage"] = $"Wystąpił nieoczekiwany błąd przy usuwaniu roli. Treść błędu: {ex.Message}";

                return View(model);
            }
        }

        private async Task CreateSubTableDataForRole(User user, UserRoleInputModel model)
        {
            var school = _context.Namespace.FirstOrDefault(x => x.Id == model.SchoolId);

            switch (model.Role)
            {
                case SystemDefaultRoles.Student:
                    SchoolStudent schoolStudent = new SchoolStudent
                    {
                        Id = Guid.NewGuid().ToString(),
                        School = school,
                        SchoolClass = null,
                        Student = user
                    };

                    _context.SchoolStudent.Add(schoolStudent);
                    break;

                case SystemDefaultRoles.Teacher:
                    SchoolTeacher schoolTeacher = new SchoolTeacher
                    {
                        Id = Guid.NewGuid().ToString(),
                        School = school,
                        SchoolClass = null,
                        Teacher = user
                    };

                    _context.SchoolTeacher.Add(schoolTeacher);
                    break;

                default:
                    return;
            }

            await _context.SaveChangesAsync();
        }

        private async Task DeleteSubTableDataForUser(User user, string role, string schoolId)
        {
            switch (role)
            {
                case SystemDefaultRoles.Student:
                    var schoolStudent = _context.SchoolStudent.FirstOrDefault(x => x.StudentId == user.Id && x.SchoolId == schoolId);

                    if (schoolStudent == null)
                    {
                        return;
                    }

                    _context.SchoolStudent.Remove(schoolStudent);
                    break;

                case SystemDefaultRoles.Teacher:
                    var schoolTeacher = _context.SchoolTeacher.FirstOrDefault(x => x.TeacherId == user.Id && x.SchoolId == schoolId);

                    if (schoolTeacher == null)
                    {
                        return;
                    }

                    _context.SchoolTeacher.Remove(schoolTeacher);
                    break;

                default:
                    return;
            }

            await _context.SaveChangesAsync();
        }
        public async Task<IActionResult> SchoolList()
        {
            return View(await _context.Namespace.ToListAsync());
        }
    }
}