﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using SzkolaXXII.Web.Models;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.ViewModel;
using System;
using System.Collections.Generic;
using SzkolaXXII.Web.Models.School;
using SzkolaXXII.Web.Data;
using System.Linq;

namespace SzkolaXXII.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly SignInManager<User> _signInManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<User> _userManager;
        private readonly SchoolContext _context;

        public HomeController(
            ILogger<HomeController> logger,
            SignInManager<User> signInManager,
            RoleManager<IdentityRole> roleManager,
            UserManager<User> userManager,
            SchoolContext context)
        {
            _logger = logger;
            _signInManager = signInManager;
            _roleManager = roleManager;
            _userManager = userManager;
            _context = context;
        }

        public IActionResult Index()
        {
            CalendarEventViewModel calendarEventViewModel = new CalendarEventViewModel
            {
                Month = DateTime.Today.Month,
                Year = DateTime.Today.Year,
                Events = GetEventList()
            };

            if (!(_signInManager.IsSignedIn(User)))
            {
                return View("StartPage");
            }
            return View(calendarEventViewModel);
        }

        private List<OneEventViewModel> GetEventList()
        {
            List<OneEventViewModel> userEvents = new List<OneEventViewModel>();

            foreach (CalendarEvent calendarEvent in _context.CalendarEvent.ToList<CalendarEvent>().Where(x => x.UserId == _userManager.GetUserId(User)))
            {
                OneEventViewModel oneEventViewModel = new OneEventViewModel
                {
                    Date = calendarEvent.Date,
                    Title = calendarEvent.Title,
                    Type = calendarEvent.Type
                };

                userEvents.Add(oneEventViewModel);
            }

            foreach (ClassCalendarEvent classCalendarEvent in _context.ClassCalendarEvent.ToList<ClassCalendarEvent>().Where(x => _context.SchoolStudent.ToList().Any(y => x.ClassId == y.SchoolClassId && y.StudentId == _userManager.GetUserId(User))))
            {
                OneEventViewModel oneEventViewModel = new OneEventViewModel
                {
                    Date = classCalendarEvent.Date,
                    Title = classCalendarEvent.Title,
                    Type = classCalendarEvent.Type
                };

                userEvents.Add(oneEventViewModel);
            }

            return userEvents;
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
