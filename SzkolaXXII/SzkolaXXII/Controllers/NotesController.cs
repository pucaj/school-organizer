﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Common.Security.Roles;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.Models.School;
using SzkolaXXII.Web.Policies;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.Controllers
{
    public class NotesController : Controller
    {
        private readonly SchoolContext _schoolContext;
        private readonly UserManager<User> _userManager;

        public NotesController(SchoolContext schoolContext, UserManager<User> userManager)
        {
            _schoolContext = schoolContext;
            _userManager = userManager;
        }

        public IActionResult Index()
        {
            var userId = _userManager.GetUserId(User);
            var notes = _schoolContext.Notes.Where(x => x.StudentId == userId);

            List<NotesViewModel> list = new List<NotesViewModel>();

            foreach (var item in notes)
            {
                list.Add(new NotesViewModel
                {
                    Notes = item,
                    User = _schoolContext.Users.FirstOrDefault(x => x.Id == item.TeacherId),
                    Color = item.Read ? "#C6C6C6" : "#FFFFF0"
                });
            }

            if (list.Count > 1) list.OrderBy(x => x.Notes.Time);

            return View(list);
        }

        public async Task<IActionResult> ShowNotes(string id)
        {
            var notes = _schoolContext.Notes.FirstOrDefault(x => x.Id == id);

            if (!notes.Read)
            {
                notes.Read = true;
                _schoolContext.Notes.Update(notes);

                await _schoolContext.SaveChangesAsync();
            }

            NotesViewModel notesViewModel = new NotesViewModel()
            {
                Notes = notes,
                User = _schoolContext.Users.FirstOrDefault(x => x.Id == notes.TeacherId),
            };

            return View(notesViewModel);
        }
    }
}