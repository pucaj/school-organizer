﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.Models.School;
using SzkolaXXII.Web.Policies;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.Controllers
{
    public class GradebookController : Controller
    {
        private readonly SchoolContext _context;
        private readonly UserManager<User> _userManager;

        public GradebookController(SchoolContext schoolContext, UserManager<User> userManager)
        {
            _context = schoolContext;
            _userManager = userManager;
        }

        [Authorize(Policy = AuthorizationPolicies.Name.AtLeastStudent)]
        public IActionResult Index(string namespaceId, string classId, string subjectId)
        {
            var userId = _userManager.GetUserId(User);
            var teachers = _context.SchoolTeacher.Where(x => x.TeacherId == userId);
            var students = _context.SchoolStudent.Where(x => x.StudentId == userId);
            var classes = new List<SchoolClass>();
            var schoolStudents = new List<StudentSelectModel>();
            var assignments = new List<Assignment>();

            if (string.IsNullOrEmpty(namespaceId))
            {
                if (teachers.Any())
                {
                    var namespaces = _context.Namespace.Where(x => teachers.Any(y => y.SchoolId == x.Id));

                    if (namespaces.Count() > 1)
                    {
                        ViewData["Schools"] = new SelectList(namespaces, "Id", "Name");
                    }
                    else
                    {
                        namespaceId = namespaces.FirstOrDefault()?.Id;
                    }
                }
                else if (students.Any())
                {
                    var namespaces = _context.Namespace.Where(x => students.Any(y => y.SchoolId == x.Id));

                    if (namespaces.Count() > 1)
                    {
                        ViewData["Schools"] = new SelectList(namespaces, "Id", "Name");
                    }
                    else
                    {
                        namespaceId = namespaces.FirstOrDefault()?.Id;
                    }
                }
                else
                {
                    return NotFound();
                }
            }

            if(teachers.Any())
            {
                classes = _context.SchoolClass.Where(x => students.Any(y => y.SchoolId == x.SchoolId)).ToList();
            }
            else
            {
                classes = _context.SchoolClass.Where(x => x.SchoolId == namespaceId).ToList();
            }


            if (!string.IsNullOrEmpty(classId))
            {
                if(students.Any(x => x.SchoolClassId != classId))
                {
                    return NotFound();
                }

                var schoolClass = _context.SchoolClass.FirstOrDefault(x => x.Id == classId);

                var subjectClasses = _context.SubjectSchoolClass
                    .Where(x => x.SchoolClassId == schoolClass.Id);

                if (!string.IsNullOrEmpty(subjectId))
                {
                    var subjectClass = subjectClasses.FirstOrDefault(x => x.SubjectId == subjectId);
                    if (subjectClass != null)
                    {
                        var subject = _context.Subject.FirstOrDefault(x => x.Id == subjectId);

                        assignments = _context.Assigment.Where(x => x.SubjectId == subjectId).ToList();

                        schoolStudents = GetStudentsModel(classId, assignments);

                        ViewData["SubjectName"] = subject == null ? "nie rozpoznano nazwy przedmiotu" : subject.Name;
                    }
                }

                var subjects = _context.Subject
                    .Where(x => subjectClasses.Any(y => y.SubjectId == x.Id))
                    .ToList();

                ViewData["Classes"] = new SelectList(classes, "Id", "Name", classes.FirstOrDefault(x => x.Id == classId));
                ViewData["Subjects"] = new SelectList(subjects, "Id", "Name", subjects.FirstOrDefault(x => x.Id == subjectId));
            }
            else
            {
                ViewData["Classes"] = new SelectList(classes, "Id", "Name");
            }

            var model = new GradebookViewModel
            {
                NamespaceId = namespaceId,
                ClassId = classId,
                SubjectId = subjectId,
                SchoolStudents = schoolStudents,
                Assignments = assignments
            };

            return View(model);
        }

        private List<StudentSelectModel> GetStudentsModel(string classId, List<Assignment> assignments)
        {
            var list = new List<StudentSelectModel>();

            var schoolClass = _context.SchoolClass.FirstOrDefault(x => x.Id == classId);
            var students = _context.SchoolStudent
                .Where(x => x.SchoolClassId == schoolClass.Id)
                .ToList();

            foreach (var student in students)
            {
                var grades = _context.SubjectGrade
                    .ToList()
                    .Where(x => x.StudentId == student.Id && assignments.Any(y => y.Id == x.SubjectId))
                    .ToList();

                list.Add(new StudentSelectModel
                {
                    Id = student.Id,
                    Email = _userManager.Users.FirstOrDefault(x => x.Id == student.StudentId)?.Email,
                    Grades = grades
                });
            }

            return list;
        }
    }
}
