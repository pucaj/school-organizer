﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.InputModel
{
    public class ClassCalendarInputModel
    {
        [Required]
        public string Title { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }

        [Required]
        public string ClassId { get; set; }

        public string SubjectId { get; set; }
    }
}
