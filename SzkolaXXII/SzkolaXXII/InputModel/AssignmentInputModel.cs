﻿using System;
using System.ComponentModel.DataAnnotations;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.InputModel
{
    public class AssignmentInputModel
    {
        public string Description { get; set; }
        public string SubjectId { get; set; }
        public string Title { get; set; }
        public string Type { get; set; }
        public int Weight { get; set; }

        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}", ApplyFormatInEditMode = true)]
        public DateTime Date { get; set; }
    }
}
