﻿using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.InputModel
{
    public class UserRoleInputModel
    {
        public AdminUserRolesViewModel ViewModel { get; set; }
        public string Role { get; set; }
        public string SchoolId { get; set; }
    }
}
