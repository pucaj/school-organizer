﻿using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.InputModel
{
    public class SchoolTeacherInputModel
    {
        public SchoolTeacherViewModel ViewModel { get; set; }
        public string Teacher { get; set; }
        public string School { get; set; }
    }
}
