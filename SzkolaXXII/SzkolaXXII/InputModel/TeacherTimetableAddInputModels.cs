﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SzkolaXXII.Web.InputModel
{
    public class TeacherTimetableAddInputModels
    {
        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public List<DateTime> StartList { get; set; }
        public List<DateTime> EndList { get; set; }

        public string Error { get; set; }

        [Required]
        public string Classroom { get; set; }

        public string DayOfWeek { get; set; }

        public List<string> DaysOfWeek { get; set; }

        public string SchoolClassId { get; set; }

        public string SubjectId { get; set; }
    }
}
