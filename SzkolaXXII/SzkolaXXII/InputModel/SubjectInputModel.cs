﻿using System.ComponentModel.DataAnnotations;
using SzkolaXXII.Web.ViewModel;

namespace SzkolaXXII.Web.InputModel
{
    public class SubjectInputModel
    {
        public SubjectViewModel ViewModel { get; set; }

        [Required(ErrorMessage = "Pole nazwy jest puste")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Pole opisu jest puste")]
        public string Description { get; set; }

        public string Id { get; set; }

        public string SchoolId { get; set; }
    }
}
