﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.InputModel
{
    public class AddClassInputModel
    {
        public string Id { get; set; }
        public List<SchoolClass> List { get; set; }
        public string ClassId { get; set; }
    }
}
