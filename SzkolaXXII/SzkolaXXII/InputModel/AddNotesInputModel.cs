﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SzkolaXXII.Web.InputModel
{
    public class AddNotesInputModel
    {
        public string StudentId { get; set; }

        public string Id { get; set; }

        [Required(ErrorMessage = "Pole tytułu jest puste")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Pole tekstu jest puste")]
        public string Text { get; set; }
    }
}
