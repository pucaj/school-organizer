﻿using System.Collections.Generic;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.ViewModel
{
    public class SubjectViewModel
    {
        public string SchoolId { get; set; }
        public List<Subject> Subjects { get; set; }
        public List<SchoolTeacher> Teachers { get; set; }
    }
}
