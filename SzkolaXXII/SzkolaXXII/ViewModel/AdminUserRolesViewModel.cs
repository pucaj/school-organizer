﻿using System.Collections.Generic;

namespace SzkolaXXII.Web.ViewModel
{
    public class AdminUserRolesViewModel
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public List<string> Roles { get; set; }
        public Dictionary<string, string> Schools { get; set; }
    }
}
