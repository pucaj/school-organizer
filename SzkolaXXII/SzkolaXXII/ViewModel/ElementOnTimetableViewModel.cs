﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.ViewModel
{
    public class ElementOnTimetableViewModel
    {
        public Subject Subject { get; set; }
        public Timetable Timetable { get; set; }
        public string TeacherName { get; set; }
        public string ClassName { get; set; }
    }
}
