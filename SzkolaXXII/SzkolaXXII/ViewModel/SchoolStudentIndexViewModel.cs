﻿using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.ViewModel
{
    public class SchoolStudentIndexViewModel
    {
        public string StudentId { get; set; }
        public User Student { get; set; }
        public string SchoolClassId { get; set; }
        public SchoolClass SchoolClass { get; set; }
    }
}
