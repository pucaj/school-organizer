﻿namespace SzkolaXXII.Web.Models.School
{
    public class AddStudentViewModel
    {
        public string Id { get; set; }
        public string SchoolClass { get; set; }
        public string StudentId { get; set; }
    }
}