﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SzkolaXXII.Web.ViewModel
{
    public class AdminIndexViewModel
    {
        public string DisplayName { get; set; }
        public string Area { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string AdditionalUrlParameters { get; set; }
    }
}
