﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.ViewModel
{
    public class SubjectSchoolClassViewModel
    {
        public List<SchoolClass> List { get; set; }
        public String Id { get; set; }
    }
}
