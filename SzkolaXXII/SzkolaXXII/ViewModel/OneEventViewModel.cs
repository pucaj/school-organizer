﻿using System;

namespace SzkolaXXII.Web.ViewModel
{
    public class OneEventViewModel
    {
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
    }
}