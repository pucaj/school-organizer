﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Models.School;
namespace SzkolaXXII.Web.ViewModel
{
    public class SubjectShowViewModel
    {
        public Subject Subject { get; set; }
        public List<Assignment> Assignments { get; set; }
    }
}
