﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Models.School;
using SzkolaXXII.Web.Areas.Identity.Data;
namespace SzkolaXXII.Web.ViewModel
{
    public class SetStudentGradeViewModel
    {
        public string AssignmentId { get; set; }
        public User User { get; set; }
        public SubjectGrade SubjectGrade { get; set; }
        public int NewGrade { get; set; }
    }
}
