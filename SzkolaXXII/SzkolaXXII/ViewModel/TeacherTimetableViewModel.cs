﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.ViewModel
{
    public class TeacherTimetableViewModel
    {
        public List<ElementOnTimetableViewModel> elementOnTimetableViewModel { get; set; }
        public string SubjectId { get; set; }
        public string Id { get; set; }
        public List<SchoolClass> SchoolClasses { get; set; }
        public string Name { get; set; }
        public bool Check { get; set; }
    }
}
