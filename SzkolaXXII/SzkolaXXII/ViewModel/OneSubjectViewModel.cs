﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.ViewModel
{
    public class OneSubjectViewModel
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public IEnumerable<SchoolClass> Classes { get; set; }
        public IEnumerable<ClassCalendarEvent> Events { get; set; }
        public IEnumerable<Assignment> Assignments { get; set; }
    }
}