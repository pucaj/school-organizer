﻿using System.Collections.Generic;

namespace SzkolaXXII.Web.ViewModel
{
    public class CalendarEventViewModel
    {
        public int Month { get; set; }
        public int Year { get; set; }
        public List<OneEventViewModel> Events { get; set; }
    }
}
