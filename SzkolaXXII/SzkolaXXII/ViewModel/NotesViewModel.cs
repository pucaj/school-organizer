﻿using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.ViewModel
{
    public class NotesViewModel
    {
        public Notes Notes { get; set; }
        public User User { get; set; }
        public string Color { get; set; }
    }
}
