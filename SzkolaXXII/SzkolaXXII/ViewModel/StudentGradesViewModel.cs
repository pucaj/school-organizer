﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SzkolaXXII.Web.ViewModel
{
    public class StudentGradesViewModel
    {
        public string AssignmentName { get; set; }
        public string AssignmentType { get; set; }
        public double Grade { get; set; }
    }
}
