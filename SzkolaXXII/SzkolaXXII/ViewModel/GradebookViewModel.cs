﻿using System.Collections.Generic;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.ViewModel
{
    public class GradebookViewModel
    {
        public string NamespaceId { get; set; }
        public string ClassId { get; set; }
        public string SubjectId { get; set; }
        public List<StudentSelectModel> SchoolStudents { get; set; }
        public List<Assignment> Assignments { get; set; }
    }

    public class StudentSelectModel
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public List<SubjectGrade> Grades { get; set; }
    }
}
