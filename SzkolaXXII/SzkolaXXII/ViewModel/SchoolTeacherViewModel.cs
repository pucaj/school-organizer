﻿using System.Collections.Generic;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.ViewModel
{
    public class SchoolTeacherViewModel
    {
        public List<User> Users { get; set; }
        public List<Namespace> Schools { get; set; }
        public List<string> UserNames { get; set; }
        public List<string> SchoolNames { get; set; }
        public List<SchoolTeacher> SchoolTeachers { get; set; }
    }
}
