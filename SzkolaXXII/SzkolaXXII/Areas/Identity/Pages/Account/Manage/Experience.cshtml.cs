using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Data;
using SzkolaXXII.Web.Models.Experience;

namespace SzkolaXXII.Web.Areas.Identity.Pages.Account.Manage
{
    [Authorize]
    public class ExperienceModel : PageModel
    {
        private readonly SchoolContext _schoolContext;
        private readonly UserManager<User> _userManager;

        public int SubjectExperienceBaseConst => 100;
        public int UserExperienceBaseConst => 300;
        public List<UserSubjectLevel> UserSubjectLevels { get; private set; }


        public ExperienceModel(SchoolContext schoolContext, UserManager<User> userManager)
        {
            _schoolContext = schoolContext;
            _userManager = userManager;
        }

        public async Task<IActionResult> OnGet()
        {
            var user = await _userManager.GetUserAsync(User);
            UserSubjectLevels = _schoolContext
                .UserSubjectLevel
                .Where(x => x.UserId == user.Id)
                .ToList();

            return Page();
        }
    }
}
