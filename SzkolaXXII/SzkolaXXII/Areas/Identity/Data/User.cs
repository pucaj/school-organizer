﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.Areas.Identity.Data
{
    public class User : IdentityUser
    {
        [Key]
        public override string Id { get; set; }
        public decimal Experience { get; set; }
        public long Level { get; set; }
    }
}
