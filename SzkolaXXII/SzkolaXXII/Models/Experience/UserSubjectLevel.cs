﻿using System.ComponentModel.DataAnnotations;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.Models.Experience
{
    public class UserSubjectLevel
    {
        [Key]
        public string Id { get; set; }
        public int Level { get; set; }
        public decimal Experience { get; set; }

        public string UserId { get; set; }
        public User User { get; set; }

        public string SubjectId { get; set; }
        public Subject Subject { get; set; }

        public static decimal CalculateNextLevel(decimal experience)
        {
            int nextLevel = NextLevelConst;
            double percent = 1.05;

            while (true)
            {
                if (experience - nextLevel >= 0)
                {
                    nextLevel += (int)(NextLevelConst * percent);
                    percent += 0.05;
                }
                else
                {
                    break;
                }
            }

            return nextLevel;
        }

        public static int CalculateLevel(decimal experience)
        {
            int level = 1;
            int nextLevel = NextLevelConst;
            double percent = 1.30;

            while (true)
            {
                if(experience - nextLevel >= 0)
                {
                    level++;
                    nextLevel += (int)(NextLevelConst * percent);
                    percent += 0.10;
                } 
                else
                {
                    break;
                }
            }

            return level;
        }

        public static int NextLevelConst => 100;
        public static double GradeConst => 4.7;
    }
}
