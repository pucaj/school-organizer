﻿using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.Experience
{
    public class ExperienceSettings
    {
        [Key]
        public string Id { get; set; }

        [Required]
        public string Key { get; set; }

        [Required]
        public string DisplayName { get; set; }

        [Required]
        public string Value { get; set; }

        public string ModifiedBy { get; set; }
        public string CreationDate { get; set; }
        public string EditionDate { get; set; }
    }
}
