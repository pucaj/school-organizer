﻿using System.ComponentModel.DataAnnotations;
using SzkolaXXII.Web.Areas.Identity.Data;
namespace SzkolaXXII.Web.Models.School
{
    public class SchoolStudent
    {
        [Key]
        [Display(Name = "Id")]
        public string Id { get; set; }

        [Display(Name = "Id Ucznia")]
        public string StudentId { get; set; }
        public User Student { get; set; }

        [Display(Name = "Id Klasy")]
        public string SchoolClassId { get; set; }
        public SchoolClass SchoolClass { get; set; }

        public string SchoolId { get; set; }
        public Namespace School { get; set; }
    }
}
