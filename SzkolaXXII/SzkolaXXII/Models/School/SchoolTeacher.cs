﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SzkolaXXII.Web.Areas.Identity.Data;

namespace SzkolaXXII.Web.Models.School
{
    public class SchoolTeacher
    {
        [Key]
        public string Id { get; set; }

        public string SchoolId { get; set; }
        public Namespace School { get; set; }
        public string TeacherId { get; set; }
        public User Teacher { get; set; }
        public SchoolClass SchoolClass { get; set; }

        public ICollection<SubjectTeacher> Subject { get; set; }
        public ICollection<SubjectGrade> SubjectGrade { get; set; }
    }
}
