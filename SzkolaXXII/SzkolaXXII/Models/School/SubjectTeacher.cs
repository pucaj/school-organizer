﻿using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class SubjectTeacher
    {
        [Key]
        public string Id { get; set; }

        public Subject Subject { get; set; }
        public string SubjectId { get; set; }
        public SchoolTeacher SchoolTeacher { get; set; }
        public string SchoolTeacherId { get; set; }
    }
}
