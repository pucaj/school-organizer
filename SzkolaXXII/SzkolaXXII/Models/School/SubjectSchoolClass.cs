﻿using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class SubjectSchoolClass
    {
        [Key]
        public string Id { get; set; }

        public string SchoolClassId { get; set; }
        public string SubjectId { get; set; }
        public SchoolClass SchoolClass { get; set; }
        public Subject Subject { get; set; }
    }
}
