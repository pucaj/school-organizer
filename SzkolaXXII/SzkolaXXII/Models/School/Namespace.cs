﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class Namespace
    {
        [Key]
        public string Id { get; set; }
        public string Address { get; set; }
        public string Name { get; set; }

        ICollection<SchoolClass> SchoolClasses { get; set; }
        ICollection<Classroom> Classrooms { get; set; }
        ICollection<Subject> Subjects { get; set; }
        ICollection<SchoolStudent> Students { get; set; }
        ICollection<SchoolTeacher> Teachers { get; set; }
    }
}
