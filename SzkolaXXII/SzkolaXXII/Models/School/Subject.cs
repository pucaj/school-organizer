﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class Subject
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string SchoolId { get; set; }
        public string TeacherId { get; set; }
        public Namespace School { get; set; }
        public ICollection<Assignment> Assignments { get; set; }
        public ICollection<SubjectTeacher> SubjectTeacher { get; set; }
        public ICollection<SubjectSchoolClass> SubjectSchoolClass { get; set; }
    }
}
