﻿using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class SubjectGrade
    {
        [Key]
        public string Id { get; set; }
        public double Grade { get; set; }
        public int Weight { get; set; }
        public string SubjectId { get; set; }
        public Assignment Subject { get; set; }
        public string StudentId { get; set; }
        public SchoolStudent Student { get; set; }
        public string TeacherId { get; set; }
        public SchoolTeacher Teacher { get; set; }
    }
}
