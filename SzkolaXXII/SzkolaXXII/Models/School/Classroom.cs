﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class Classroom
    {
        [Key]
        public string Id { get; set; }
        public string Localization { get; set; }
        public string Description { get; set; }

        public Namespace Namespace { get; set; }
    }
}