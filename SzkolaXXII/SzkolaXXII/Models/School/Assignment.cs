﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class Assignment
    {
        [Key]
        public string Id { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public int Weight { get; set; }
        public DateTime Date { get; set; }
        public string SubjectId { get; set; }
        public Subject Subject { get; set; }
    }
}
