﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class SchoolClass
    {
        [Key]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        [Required]
        public DateTime StartSchoolDate { get; set; }

        [Required]
        public DateTime EndSchoolDate { get; set; }

        [Required]
        public string EducatorId { get; set; }   
        public SchoolTeacher Educator { get; set; }

        [Required]
        public string SchoolId { get; set; }
        public Namespace School { get; set; }

        public ICollection<SchoolStudent> Students { get; set; }
        public ICollection<SubjectSchoolClass> SubjectSchoolClass { get; set; }
    }
}
