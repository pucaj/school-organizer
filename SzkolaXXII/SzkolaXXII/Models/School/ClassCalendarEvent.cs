﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class ClassCalendarEvent
    {
        [Key]
        public string Id { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public string ClassId { get; set; }
        public Classroom Classroom { get; set; }
    }
}