﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class CalendarEvent
    {
        [Key]
        public string Id { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string Type { get; set; }
        public string UserId { get; set; }
    }
}