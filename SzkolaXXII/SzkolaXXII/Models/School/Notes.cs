﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using SzkolaXXII.Web.Areas.Identity.Data;

namespace SzkolaXXII.Web.Models.School
{
    public class Notes
    {
        [Key]
        public string Id { get; set; }

        public string TeacherId { get; set; }
        public User Teacher { get; set; }

        public string StudentId { get; set; }
        public User Student { get; set; }

        public DateTime Time { get; set; }
        public bool Read { get; set; }

        [MaxLength(100)]
        public string Title { get; set; }

        [MaxLength(250)]
        public string Text { get; set; }
    }
}
