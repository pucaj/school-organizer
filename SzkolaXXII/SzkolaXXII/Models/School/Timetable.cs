﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SzkolaXXII.Web.Models.School
{
    public class Timetable
    {
        [Key]
        public string Id { get; set; }
        public string DayofWeek { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Classroom { get; set; }

        public string SchoolClassId { get; set; }
        public SchoolClass SchoolClass { get; set; }

        public string SubjectId { get; set; }
        public Subject Subject { get; set; }
    }
}
