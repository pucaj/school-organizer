﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SzkolaXXII.Web.Migrations
{
    public partial class EditAssignmentMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "Date",
                table: "Assigment",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Title",
                table: "Assigment",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Date",
                table: "Assigment");

            migrationBuilder.DropColumn(
                name: "Title",
                table: "Assigment");
        }
    }
}
