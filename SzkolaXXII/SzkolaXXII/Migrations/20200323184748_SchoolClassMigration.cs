﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SzkolaXXII.Web.Migrations
{
    public partial class SchoolClassMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SchoolClass_SchoolTeacher_EducatorId",
                table: "SchoolClass");

            migrationBuilder.DropForeignKey(
                name: "FK_SchoolClass_Namespace_SchoolId",
                table: "SchoolClass");

            migrationBuilder.DropIndex(
                name: "IX_SchoolClass_EducatorId",
                table: "SchoolClass");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartSchoolDate",
                table: "SchoolClass",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "SchoolId",
                table: "SchoolClass",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "SchoolClass",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndSchoolDate",
                table: "SchoolClass",
                nullable: false,
                oldClrType: typeof(DateTime),
                oldType: "datetime2",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "EducatorId",
                table: "SchoolClass",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(450)",
                oldNullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_SchoolClass_EducatorId",
                table: "SchoolClass",
                column: "EducatorId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_SchoolClass_SchoolTeacher_EducatorId",
                table: "SchoolClass",
                column: "EducatorId",
                principalTable: "SchoolTeacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SchoolClass_Namespace_SchoolId",
                table: "SchoolClass",
                column: "SchoolId",
                principalTable: "Namespace",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SchoolClass_SchoolTeacher_EducatorId",
                table: "SchoolClass");

            migrationBuilder.DropForeignKey(
                name: "FK_SchoolClass_Namespace_SchoolId",
                table: "SchoolClass");

            migrationBuilder.DropIndex(
                name: "IX_SchoolClass_EducatorId",
                table: "SchoolClass");

            migrationBuilder.AlterColumn<DateTime>(
                name: "StartSchoolDate",
                table: "SchoolClass",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "SchoolId",
                table: "SchoolClass",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "SchoolClass",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<DateTime>(
                name: "EndSchoolDate",
                table: "SchoolClass",
                type: "datetime2",
                nullable: true,
                oldClrType: typeof(DateTime));

            migrationBuilder.AlterColumn<string>(
                name: "EducatorId",
                table: "SchoolClass",
                type: "nvarchar(450)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.CreateIndex(
                name: "IX_SchoolClass_EducatorId",
                table: "SchoolClass",
                column: "EducatorId",
                unique: true,
                filter: "[EducatorId] IS NOT NULL");

            migrationBuilder.AddForeignKey(
                name: "FK_SchoolClass_SchoolTeacher_EducatorId",
                table: "SchoolClass",
                column: "EducatorId",
                principalTable: "SchoolTeacher",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_SchoolClass_Namespace_SchoolId",
                table: "SchoolClass",
                column: "SchoolId",
                principalTable: "Namespace",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
