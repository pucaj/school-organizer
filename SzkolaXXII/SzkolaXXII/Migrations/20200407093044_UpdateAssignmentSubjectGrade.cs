﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SzkolaXXII.Web.Migrations
{
    public partial class UpdateAssignmentSubjectGrade : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Type",
                table: "Assigment",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Weight",
                table: "Assigment",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Type",
                table: "Assigment");

            migrationBuilder.DropColumn(
                name: "Weight",
                table: "Assigment");
        }
    }
}
