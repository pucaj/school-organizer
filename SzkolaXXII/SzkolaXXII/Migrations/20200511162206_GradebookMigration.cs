﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SzkolaXXII.Web.Migrations
{
    public partial class GradebookMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SchoolClassAssignment");

            migrationBuilder.DropIndex(
                name: "IX_SubjectGrade_SubjectId",
                table: "SubjectGrade");

            migrationBuilder.CreateIndex(
                name: "IX_SubjectGrade_SubjectId",
                table: "SubjectGrade",
                column: "SubjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropIndex(
                name: "IX_SubjectGrade_SubjectId",
                table: "SubjectGrade");

            migrationBuilder.CreateTable(
                name: "SchoolClassAssignment",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    AssignmentId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    SchoolClassId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SchoolClassAssignment", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SchoolClassAssignment_Assigment_AssignmentId",
                        column: x => x.AssignmentId,
                        principalTable: "Assigment",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_SchoolClassAssignment_SchoolClass_SchoolClassId",
                        column: x => x.SchoolClassId,
                        principalTable: "SchoolClass",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SubjectGrade_SubjectId",
                table: "SubjectGrade",
                column: "SubjectId",
                unique: true,
                filter: "[SubjectId] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_SchoolClassAssignment_AssignmentId",
                table: "SchoolClassAssignment",
                column: "AssignmentId");

            migrationBuilder.CreateIndex(
                name: "IX_SchoolClassAssignment_SchoolClassId",
                table: "SchoolClassAssignment",
                column: "SchoolClassId");
        }
    }
}
