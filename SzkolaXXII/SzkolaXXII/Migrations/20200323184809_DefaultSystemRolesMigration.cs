﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SzkolaXXII.Web.Migrations
{
    public partial class DefaultSystemRolesMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                INSERT INTO AspNetRoles(Id, Name, NormalizedName, ConcurrencyStamp)
                VALUES (NEWID(), 'Administrator', 'ADMINISTRATOR', NEWID()),
                       (NEWID(), 'Moderator', 'MODERATOR', NEWID()),
                       (NEWID(), 'Nauczyciel', 'NAUCZYCIEL', NEWID()),
                       (NEWID(), 'Student', 'STUDENT', NEWID())
            ");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql(@"
                DELETE FROM AspNetRoles
                WHERE Name IN ('Administrator', 'Moderator', 'Nauczyciel', 'Student')
            ");
        }
    }
}
