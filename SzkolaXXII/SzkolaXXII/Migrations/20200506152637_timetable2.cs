﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SzkolaXXII.Web.Migrations
{
    public partial class timetable2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "DayofWeek",
                table: "Timetable",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DayofWeek",
                table: "Timetable");
        }
    }
}
