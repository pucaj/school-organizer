﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SzkolaXXII.Web.Migrations
{
    public partial class Timetable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TimetableItem");

            migrationBuilder.AddColumn<string>(
                name: "Classroom",
                table: "Timetable",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "EndTime",
                table: "Timetable",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "StartTime",
                table: "Timetable",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "SubjectId",
                table: "Timetable",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Timetable_SubjectId",
                table: "Timetable",
                column: "SubjectId");

            migrationBuilder.AddForeignKey(
                name: "FK_Timetable_Subject_SubjectId",
                table: "Timetable",
                column: "SubjectId",
                principalTable: "Subject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Timetable_Subject_SubjectId",
                table: "Timetable");

            migrationBuilder.DropIndex(
                name: "IX_Timetable_SubjectId",
                table: "Timetable");

            migrationBuilder.DropColumn(
                name: "Classroom",
                table: "Timetable");

            migrationBuilder.DropColumn(
                name: "EndTime",
                table: "Timetable");

            migrationBuilder.DropColumn(
                name: "StartTime",
                table: "Timetable");

            migrationBuilder.DropColumn(
                name: "SubjectId",
                table: "Timetable");

            migrationBuilder.CreateTable(
                name: "TimetableItem",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    BreakCount = table.Column<int>(type: "int", nullable: false),
                    ClassroomId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    EndTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    StartTime = table.Column<DateTime>(type: "datetime2", nullable: false),
                    SubjectId = table.Column<string>(type: "nvarchar(450)", nullable: true),
                    TimetableId = table.Column<string>(type: "nvarchar(450)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TimetableItem", x => x.Id);
                    table.ForeignKey(
                        name: "FK_TimetableItem_Classroom_ClassroomId",
                        column: x => x.ClassroomId,
                        principalTable: "Classroom",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TimetableItem_Subject_SubjectId",
                        column: x => x.SubjectId,
                        principalTable: "Subject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_TimetableItem_Timetable_TimetableId",
                        column: x => x.TimetableId,
                        principalTable: "Timetable",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_TimetableItem_ClassroomId",
                table: "TimetableItem",
                column: "ClassroomId");

            migrationBuilder.CreateIndex(
                name: "IX_TimetableItem_SubjectId",
                table: "TimetableItem",
                column: "SubjectId");

            migrationBuilder.CreateIndex(
                name: "IX_TimetableItem_TimetableId",
                table: "TimetableItem",
                column: "TimetableId");
        }
    }
}
