﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SzkolaXXII.Web.Areas.Identity.Data;
using SzkolaXXII.Web.Models.Experience;
using SzkolaXXII.Web.Models.School;

namespace SzkolaXXII.Web.Data
{
    public class SchoolContext : IdentityDbContext<User>
    {
        public SchoolContext(DbContextOptions<SchoolContext> options)
            : base(options)
        {
            
        }

        public DbSet<Assignment> Assigment { get; set; }
        public DbSet<SchoolClass> Classroom { get; set; }
        public DbSet<CalendarEvent> CalendarEvent { get; set; }
        public DbSet<ClassCalendarEvent> ClassCalendarEvent { get; set; }
        public DbSet<Namespace> Namespace { get; set; }
        public DbSet<Notes> Notes { get; set; }
        public DbSet<SchoolClass> SchoolClass { get; set; }
        public DbSet<SchoolStudent> SchoolStudent { get; set; }
        public DbSet<SchoolTeacher> SchoolTeacher { get; set; }
        public DbSet<Subject> Subject { get; set; }
        public DbSet<SubjectGrade> SubjectGrade { get; set; }
        public DbSet<SubjectSchoolClass> SubjectSchoolClass { get; set; }
        public DbSet<SubjectTeacher> SubjectTeacher { get; set; }
        public DbSet<Timetable> Timetable { get; set; }
        public DbSet<UserSubjectLevel> UserSubjectLevel { get; set; }
        public DbSet<ExperienceSettings> ExperienceSettings { get; set; }
    }
}
