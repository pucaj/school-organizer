﻿using Microsoft.AspNetCore.Authorization;
using System.Collections.Generic;
using System.Linq;
using SzkolaXXII.Common.Security.Roles;

namespace SzkolaXXII.Web.Policies
{
    public static class AuthorizationPolicies
    {
        public static class Name
        {
            public const string AtLeastStudent = "AtLeastStudent";
            public const string AtLeastTeacher = "AtLeastTeacher";
            public const string AtLeastModerator = "AtLeastModerator";
        }

        public static AuthorizationPolicy AtLeastStudent =>
            new AuthorizationPolicyBuilder()
                .RequireRole(new string[]
                {
                    SystemDefaultRoles.Student,
                    SystemDefaultRoles.Teacher,
                    SystemDefaultRoles.Moderator,
                    SystemDefaultRoles.Administrator
                })
                .Build();

        public static AuthorizationPolicy AtLeastTeacher =>
            new AuthorizationPolicyBuilder()
                .RequireRole(new string[]
                {
                    SystemDefaultRoles.Teacher,
                    SystemDefaultRoles.Moderator,
                    SystemDefaultRoles.Administrator
                })
                .Build();

        public static AuthorizationPolicy AtLeastModerator =>
            new AuthorizationPolicyBuilder()
                .RequireRole(new string[]
                {
                    SystemDefaultRoles.Moderator,
                    SystemDefaultRoles.Administrator
                })
                .Build();

        /// <summary>
        /// Build AuthorizationPolicy with parameter roles
        /// </summary>
        /// <param name="roles">Roles to build</param>
        /// <returns>AuthorizationPolicy</returns>
        public static AuthorizationPolicy CreateRoles(string[] roles) =>
            new AuthorizationPolicyBuilder()
                .RequireRole(roles)
                .Build();

        public static AuthorizationPolicy CreateClaim(string claimType, params string[] allowedValues) =>
            new AuthorizationPolicyBuilder()
                .RequireClaim(claimType, allowedValues)
                .Build();

        public static AuthorizationPolicy CreateClaim(string claimType, IEnumerable<string> allowedValues) =>
            new AuthorizationPolicyBuilder()
                .RequireClaim(claimType, allowedValues)
                .Build();

        public static AuthorizationPolicy CreateClaim(string claimType) =>
            new AuthorizationPolicyBuilder()
                .RequireClaim(claimType)
                .Build();

        /// <summary>
        /// Build AuthorizationPolicy from default system roles except roles from parameter.
        /// </summary>
        /// <param name="roles">Roles to skip</param>
        /// <returns>AuthorizationPolicy</returns>
        public static AuthorizationPolicy Except(string[] roles) =>
            new AuthorizationPolicyBuilder()
                .RequireRole(new string[]
                {
                    SystemDefaultRoles.Student,
                    SystemDefaultRoles.Teacher,
                    SystemDefaultRoles.Moderator,
                    SystemDefaultRoles.Administrator
                }.Except(roles))
                .Build();
    }
}
